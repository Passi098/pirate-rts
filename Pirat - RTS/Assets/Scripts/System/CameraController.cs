﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour  //Done by: Pascal
{
    private static CameraController m_Instance;
    [SerializeField]
    private float m_Speed = 10;
    [SerializeField]
    private float m_RotationSpeed = 50;
    [SerializeField]
    private float m_ZoomSpeed = 50;
    [SerializeField]
    private float m_SpeedMultiplier = 1;
    private const float CAM_MOVEMENT_SPEED_MULTIPLIER = 100;
    private const float CAM_ZOOM_SPEED_MULTIPLIER = 100;

    private Transform Mover
    {
        get { return transform.parent.parent; }
    }    
    
    private Transform Rotator
    {
        get { return transform.parent; }
    }

    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(gameObject);
        }
        m_Instance = this;
    }

    public static Vector2 SetPosition
    {
        set { m_Instance.Mover.position = new Vector3(value.x, m_Instance.Mover.position.y, value.y); }
    }

    private void Update()
    {
        Movement();
        RotationY();
        RotationX();
        Zoom();
    }

    private void Zoom()
    {
        if (Input.GetKey(KeyBindings.m_CamSpeedUp))
        {
            m_SpeedMultiplier = CAM_ZOOM_SPEED_MULTIPLIER;
        }
        transform.position += Rotator.forward * m_ZoomSpeed * m_SpeedMultiplier * Time.deltaTime * Input.mouseScrollDelta.y;
        if(transform.localPosition.z > -30)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -30);
        }
        else if (transform.localPosition.z < -250)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -250);
        }
        m_SpeedMultiplier = 1;
    }

    /// <summary>
    /// Rotation of the camera along x-axis
    /// </summary>
    private void RotationX()
    {
        if (Input.GetKey(KeyBindings.m_CamRotate))
        {
            Rotator.Rotate(m_RotationSpeed * Time.deltaTime * -Input.GetAxis("Mouse Y"), 0, 0);
            if(Rotator.rotation.eulerAngles.x < 0 || Rotator.rotation.eulerAngles.x > 180)
            {
                Rotator.rotation = Quaternion.Euler(0, Rotator.rotation.eulerAngles.y, Rotator.rotation.eulerAngles.z);
            }
            else if (Rotator.rotation.eulerAngles.x > 80)
            {
                Rotator.rotation = Quaternion.Euler(80, Rotator.rotation.eulerAngles.y, Rotator.rotation.eulerAngles.z);
            }
        }
    }

    /// <summary>
    /// Rotation of the camera along y-axis
    /// </summary>
    private void RotationY()
    {
        if (Input.GetKey(KeyBindings.m_CamRotate))
        {
            Mover.Rotate(0, m_RotationSpeed * Time.deltaTime * Input.GetAxis("Mouse X"), 0);
        }
    }

    /// <summary>
    /// Movement, to move... ment
    /// </summary>
    private void Movement()
    {
        if(Input.GetKey(KeyBindings.m_CamSpeedUp))
        {
            m_SpeedMultiplier = CAM_MOVEMENT_SPEED_MULTIPLIER;
        }
        if (Input.GetKey(KeyBindings.m_CamForward))
        {
            Mover.position += Mover.forward * m_Speed * m_SpeedMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyBindings.m_CamBackward))
        {
            Mover.position -= Mover.forward * m_Speed * m_SpeedMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyBindings.m_CamLeft))
        {
            Mover.position -= Mover.right * m_Speed * m_SpeedMultiplier * Time.deltaTime;
        }
        if (Input.GetKey(KeyBindings.m_CamRight))
        {
            Mover.position += Mover.right * m_Speed * m_SpeedMultiplier * Time.deltaTime;
        }
        if(Mover.position.x < 0)
        {
            Mover.position = new Vector3(0, Mover.position.y, Mover.position.z);
        }
        else if(Mover.position.x > 511 * 25)
        {
            Mover.position = new Vector3(511 * 25, Mover.position.y, Mover.position.z);
        }
        if (Mover.position.z < 0)
        {
            Mover.position = new Vector3( Mover.position.x, Mover.position.y, 0);
        }
        else if (Mover.position.z > 511 * 25)
        {
            Mover.position = new Vector3(Mover.position.x, Mover.position.y, 511 * 25);
        }
        m_SpeedMultiplier = 1;
    }
}
