﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[DisallowMultipleComponent]
public class Selecter : MonoBehaviour  //Done by: Pascal
{
    private static Selecter m_Instance;

    public readonly static List<Selectable> m_SelectedUnits = new List<Selectable>();

    public static float m_WaterHeight = 0;

    [SerializeField]
    private Texture2D m_CursorDestination;
    [SerializeField]
    private Texture2D m_CursorAttack;

    private void Awake()
    {
        if (m_Instance != null)
        {
            Destroy(this);
        }
        if (gameObject != Camera.main.transform.gameObject)
        {
            if (Camera.main == null)
            {
                return;
            }
            Camera.main.gameObject.AddComponent<Selecter>();
            Destroy(this);
        }
        else
        {
            m_Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyBindings.m_SelecterSelect))
        {
            Select();
        }
        if (Input.GetKeyDown(KeyBindings.m_SelecterSetDestination))
        {
            ControllSelected();
        }
    }

    private void FixedUpdate()
    {
        if(IsCursorOverUI)
        {
            Cursor.SetCursor(default, Vector2.zero, CursorMode.Auto);
            return;
        }
        if (m_SelectedUnits.Count > 0)
        {
            if(m_SelectedUnits[0].SelectableType == GameRegistry.ESelectableType.Shop || m_SelectedUnits[0].Fraction != GameRegistry.EFractions.Pirates)
            {
                Cursor.SetCursor(default, Vector2.zero, CursorMode.Auto);
                return;
            }
            if (HitRayASelectable(out Selectable sa))
            {
                if (sa.SelectableType == GameRegistry.ESelectableType.Ship)
                {
                    if (sa.Fraction == GameRegistry.EFractions.British || sa.Fraction == GameRegistry.EFractions.Neutral)
                    {
                        //change cursor image to attack image
                        Cursor.SetCursor(m_CursorAttack, Vector2.zero, CursorMode.Auto);
                    }
                }
                else
                {
                    //cursor to destination cursor
                    Cursor.SetCursor(m_CursorDestination, Vector2.zero, CursorMode.Auto);
                }
            }
            else
            {
                //cursor to default
                Cursor.SetCursor(m_CursorDestination, Vector2.zero, CursorMode.Auto);
            }
        }
        else
        {
            //cursor to default
            Cursor.SetCursor(default, Vector2.zero, CursorMode.Auto);
        }
    }

    private bool IsCursorOverUI
    {
        get { return EventSystem.current.IsPointerOverGameObject(); }
    }

    private void Select()
    {
        if(IsCursorOverUI)
        {
            return;
        }
        if (HitRayASelectable(out Selectable ca))
        {
            if (ca.SelectableType == GameRegistry.ESelectableType.Shop)
            {
                for (int i = 0; i < m_SelectedUnits.Count; i++)
                {
                    m_SelectedUnits[i].OutlineActive = false;
                }
                m_SelectedUnits.Clear();
                if (ca.Fraction == GameRegistry.EFractions.Pirates)
                {
                    BuyMenu.m_ActiveShop = (Shop)ca;
                    BuyMenu.Open();
                }
            }
            else
            {
                BuyMenu.Close();
                BuyMenu.m_ActiveShop = null;
                if (ca.Fraction == GameRegistry.EFractions.Pirates)
                {
                    if (m_SelectedUnits.Contains(ca))
                    {
                        if (Input.GetKey(KeyBindings.m_SelecterMultiSelection))
                        {
                            SelectedObjectUI.Activate(m_SelectedUnits.ToArray(), m_SelectedUnits.IndexOf(ca));
                            return;
                        }
                        else
                        {
                            for (int i = 0; i < m_SelectedUnits.Count; i++)
                            {
                                m_SelectedUnits[i].OutlineActive = false;
                            }
                            m_SelectedUnits.Clear();
                        }
                    }
                    else
                    {
                        if (!Input.GetKey(KeyCode.LeftControl))
                        {
                            for (int i = 0; i < m_SelectedUnits.Count; i++)
                            {
                                m_SelectedUnits[i].OutlineActive = false;
                            }
                            m_SelectedUnits.Clear();
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < m_SelectedUnits.Count; i++)
                    {
                        m_SelectedUnits[i].OutlineActive = false;
                    }
                    m_SelectedUnits.Clear();
                }
            }
            ca.OutlineActive = true;
            m_SelectedUnits.Add(ca);
            SelectedObjectUI.Activate(m_SelectedUnits.ToArray(), m_SelectedUnits.IndexOf(ca));
        }
        else
        {
            BuyMenu.Close();
            BuyMenu.m_ActiveShop = null;
            for (int i = 0; i < m_SelectedUnits.Count; i++)
            {
                m_SelectedUnits[i].OutlineActive = false;
            }
            m_SelectedUnits.Clear();
        }
    }

    private bool ShootRaycast(out RaycastHit hit)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        return Physics.Raycast(ray, out hit);
    }

    private bool HitRayASelectable(out Selectable sa)
    {
        if (ShootRaycast(out RaycastHit hit))
        {
            sa = hit.transform.gameObject.GetComponent<Selectable>();
            if (sa != null)
            {
                return true;
            }
        }
        sa = null;
        return false;
    }

    private void ControllSelected()
    {
        if (HitRayASelectable(out Selectable sa))
        {
            if (sa.SelectableType == GameRegistry.ESelectableType.Ship)
            {
                if (sa.Fraction == GameRegistry.EFractions.Pirates)
                {
                    AllMove(sa.transform.position);
                }
                else
                {
                    AllAttack(sa);
                }
            }
            return;
        }
        if (GetRayHitOnWater(out Vector3 destination))
        {
            AllMove(destination);
        }
    }

    private bool GetRayHitOnWater(out Vector3 _position)
    {
        _position = Vector3.zero;
        Vector3 direction = Camera.main.ScreenPointToRay(Input.mousePosition).direction;
        if (direction.y >= 0)
        {
            return false;
        }
        _position = Camera.main.transform.position + (direction * ((m_WaterHeight - Camera.main.transform.position.y) / direction.y));
        return true;
    }

    private void AllMove(Vector3 _destination)
    {
        for (int i = 0; i < m_SelectedUnits.Count; i++)
        {
            //if(m_SelectedUnits[i].m_OnDestroyGarnision != null)
            //{
            //    m_SelectedUnits[i].m_OnDestroyGarnision(m_SelectedUnits[i], GameRegistry.EFractions.Neutral);
            //}
            if (m_SelectedUnits[i].Fraction == GameRegistry.EFractions.Pirates)
            {
                m_SelectedUnits[i].Move(_destination);
                ShareBoidReferences();
            }
        }
    }

    public void ShareBoidReferences()
    {
        List<ShipBehaviour> boids = new List<ShipBehaviour>();
        for (int i = 0; i < m_SelectedUnits.Count; i++)
        {
            boids.Add(m_SelectedUnits[i].Boid);
        }
        for (int i = 0; i < m_SelectedUnits.Count; i++)
        {
            m_SelectedUnits[i].ShipGroup = boids;
        }
    }

    private void AllAttack(Selectable _target)
    {
        for (int i = 0; i < m_SelectedUnits.Count; i++)
        {
            //if (m_SelectedUnits[i].m_OnDestroyGarnision != null)
            //{
            //    m_SelectedUnits[i].m_OnDestroyGarnision(m_SelectedUnits[i], GameRegistry.EFractions.Neutral);
            //}
            if (m_SelectedUnits[i].Fraction == GameRegistry.EFractions.Pirates)
            {
                m_SelectedUnits[i].Attack(_target);
            }
        }
    }
}
