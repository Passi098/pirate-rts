﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightManager //Lukas
{
    public List<ShipBehaviour> m_FightingShips = new List<ShipBehaviour>();
    public Vector2 m_FightCenter;
    public Vector3 m_FightCenter3D;
    public List<Isle> m_Isles = new List<Isle>();
    public float m_SqrdMaxDistanceToCenter { get; private set; } = 50000;

    /// <summary>
    /// Use all ships positions participating in a fight, to calculate the fights center point
    /// </summary>
    public void CalcFightCenter()
    {
        Vector3 center = Vector3.zero;
        for (int i = 0; i < m_FightingShips.Count; i++)
        {
            center += m_FightingShips[i].transform.position;
        }
        center /= m_FightingShips.Count;
        m_FightCenter = new Vector2(center.x, center.z);
        m_FightCenter3D = center;
        for (int i = 0; i < Isle.m_Isles.Count; i++)
        {
            if((Isle.m_Isles[i].m_IsleCenter - m_FightCenter).sqrMagnitude <= m_SqrdMaxDistanceToCenter)
            {
                m_Isles.Add(Isle.m_Isles[i]);
            }
        }
    }

}
