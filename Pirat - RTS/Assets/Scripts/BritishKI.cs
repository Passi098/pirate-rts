﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ShipBehaviour))]
public class BritishKI : MonoBehaviour //Lukas
{
    private List<ShipBehaviour> m_PotentialTargetList = new List<ShipBehaviour>();

    private Vector3[] m_PatrolWayPoints;
    private Coroutine m_PatrolCR;

    [SerializeField]
    private float m_ScanRange;
    [SerializeField]
    private float m_WaitTime;
    [SerializeField]
    private float m_SqrdDist = 25f; //distance to check if waypoint is reached
    [SerializeField]
    private LayerMask m_ShipMask;

    private ShipBehaviour m_ShipBehaviour;
    private ShipBehaviour m_CurrentTarget;

    private Isle m_HomeIsland;

    private int m_Waypoints = 8;

    private bool m_IsPatroling = true;

    public Isle HomeIsland
    {
        set
        {
            m_HomeIsland = value;
            CalcPatrolWaypoint(m_HomeIsland);
            Debug.Log("is calced");
        }
    }

    private void Awake()
    {
        m_PatrolWayPoints = new Vector3[m_Waypoints];


        m_ShipBehaviour = GetComponent<ShipBehaviour>();
        m_ShipBehaviour.m_BattleBeginCallback = StopPatroling; //Callbacks for start and ending fightbehaviour
        m_ShipBehaviour.m_BattleEndCallback = StartPatroling;

        //CalcPatrolWaypoint(m_HomeIsland);
        //m_PatrolCR = StartCoroutine(PatrolAlongWaypoints());
    }

    void FixedUpdate()
    {
        ScanForEnemies();
    }

    /// <summary>
    /// Stop patroling routine, when going over to other bahviour
    /// </summary>
    private void StopPatroling()
    {
        if (m_PatrolCR != null)
        {
            StopCoroutine(m_PatrolCR);
        }
    }

    /// <summary>
    /// Start patroling again, after ending another behaviour
    /// </summary>
    public void StartPatroling()
    {
        m_PatrolCR = StartCoroutine(PatrolAlongWaypoints());
    }

    /// <summary>
    /// Scan for collider with ship mask, get their shipbehaviour component and list them up as potential targets
    /// </summary>
    private void ScanForEnemies()
    {
        if (m_CurrentTarget != null) //check if there is already 
        {
            return;
        }
        else
        {
            Collider[] ships = Physics.OverlapSphere(transform.position, m_ScanRange, m_ShipMask); //get all ship collider in scan range
            for (int i = 0; i < ships.Length; i++)
            {
                ShipBehaviour ship = ships[i].GetComponent<ShipBehaviour>();
                if (ship.Fraction == GameRegistry.EFractions.Pirates) //check if ship is enemy fraction (pirate)
                {
                    m_PotentialTargetList.Add(ship);
                }
            }
            if (m_CurrentTarget == null && m_PotentialTargetList.Count > 0) // if there is no current target, put in the first found (should be the nearest)
            {
                m_CurrentTarget = m_PotentialTargetList[0];
                m_PotentialTargetList.RemoveAt(0); //be sure to remove it from list, to not risk bugs
                m_ShipBehaviour.Attack(m_CurrentTarget); //initiate attack method with current target
            }
        }
    }

    /// <summary>
    /// Calculate Waypoints relative from given home island
    /// </summary>
    /// <param name="_homeIsland"> transform </param>
    private void CalcPatrolWaypoint(Isle _homeIsland)
    {
        Vector3 center = new Vector3(_homeIsland.m_IsleCenter.x, 0, _homeIsland.m_IsleCenter.y);
        m_PatrolWayPoints[0] = Vector3.forward * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[1] = (Vector3.forward + Vector3.right).normalized * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[2] = Vector3.right * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[3] = (-Vector3.forward + Vector3.right).normalized * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[4] = -Vector3.forward * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[5] = (-Vector3.forward - Vector3.right).normalized * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[6] = -Vector3.right * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
        m_PatrolWayPoints[7] = (Vector3.forward - Vector3.right).normalized * ((center - transform.position).magnitude * 1 + Random.Range(100f, 150f)) + center;
    }

    /// <summary>
    /// Patrol through every waypoint and shortly stop at every point before continuing
    /// </summary>
    /// <returns></returns>
    private IEnumerator PatrolAlongWaypoints()
    {
        m_IsPatroling = true;
        if (!(m_PatrolWayPoints.Length > 0))
        {
            CalcPatrolWaypoint(m_HomeIsland);
        }
        while (m_IsPatroling)
        {
            for (int i = 0; i < m_PatrolWayPoints.Length; i++)
            {
                m_ShipBehaviour.Move(m_PatrolWayPoints[i]);

                while ((transform.position - m_PatrolWayPoints[i]).sqrMagnitude >= m_SqrdDist)
                {
                    yield return null;
                }
                yield return new WaitForSeconds(m_WaitTime);
            }
        }
    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.green;
        //Gizmos.DrawWireSphere(transform.position, m_ScanRange);

        //Gizmos.color = Color.blue;
        //Gizmos.DrawLine(m_HomeIsland.position, transform.position);

        //Gizmos.color = Color.cyan;
        //Gizmos.DrawLine(Vector3.zero, transform.position);

        //Gizmos.color = Color.green;
        //Gizmos.DrawLine(Vector3.zero, m_HomeIsland.position);

        //Gizmos.color = Color.black;

        //for (int i = 0; i < m_Waypoints; i++)
        //{
        //    Gizmos.DrawWireSphere(m_PatrolWayPoints[i], 5f);
        //}
    }
}
