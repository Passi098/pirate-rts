﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MeshGenerator //Done by: Pascal
{


    public static Mesh GenerateMesh(float _verteciesDistanceX, float _verteciesDistanceY, float[,] _heightmap)
    {
        Mesh mesh = new Mesh();
        mesh.Clear();

        Vector3 vertex = Vector3.zero;
        Vector3[] vertecies = new Vector3[_heightmap.Length];

        for (int z = 0, counter = 0; z < _heightmap.GetLength(1); z++)
        {
            for (int x = 0; x < _heightmap.GetLength(0); x++, counter++)
            {
                vertex.x = x * _verteciesDistanceX;
                vertex.y = _heightmap[x, z];
                vertex.z = z * _verteciesDistanceY;

                vertecies[counter] = vertex;
            }
        }

        int[] triangles = new int[(((_heightmap.GetLength(0) - 1) * (_heightmap.GetLength(1) - 1)) * 2) * 3];

        for (int z = 0, counter = 0; z < _heightmap.GetLength(1) - 1; z++)
        {
            for (int x = 0; x < _heightmap.GetLength(0) - 1; x++, counter += 6)
            {
                triangles[counter] = (z * _heightmap.GetLength(0)) + x;
                triangles[counter + 1] = triangles[counter] + _heightmap.GetLength(0) + 1;
                triangles[counter + 2] = triangles[counter] + 1;

                //Debug.Log(triangles[counter] + "," + triangles[counter + 1] + "," + triangles[counter + 2]);

                triangles[counter + 3] = triangles[counter];
                triangles[counter + 4] = triangles[counter] + _heightmap.GetLength(0);
                triangles[counter + 5] = triangles[counter + 1];


                //Debug.Log(triangles[counter + 3] + "," + triangles[counter + 4] + "," + triangles[counter + 5]);
            }
        }

        mesh.vertices = vertecies;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.name = "Mesh";
        return mesh;
    }
}
