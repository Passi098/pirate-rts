﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ShipRegistry : ScriptableObject //Done by: Pascal
{
    public GameObject Shallop;
    public GameObject Cannonboat;
    public GameObject Galleon;
    public GameObject Flagship;

    public GameObject NeutralShallop;
    public GameObject NeutralCannonboat;
    public GameObject NeutralGalleon;
    public GameObject NeutralFlagship;

    public GameObject BritishShallop;
    public GameObject BritishCannonboat;
    public GameObject BritishGalleon;
    public GameObject BritishFlagship;
}
