﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Outline))]
public class Selectable : MonoBehaviour //Done by: Pascal
{
    private Outline m_Outline;

    private void Awake()
    {
        m_Outline = GetComponent<Outline>();
        SelectableAwake();
    }

    protected virtual void SelectableAwake()
    {

    }

    public virtual GameRegistry.EFractions Fraction
    {
        get { return GameRegistry.EFractions.Pirates; }
    }

    public virtual GameRegistry.ESelectableType SelectableType
    {
        get { return GameRegistry.ESelectableType.Ship; }
    }

    public virtual ShipBehaviour Boid { get { return null; } }

    public virtual List<ShipBehaviour> ShipGroup { set { } }

    public bool OutlineActive
    {
        get { return m_Outline.enabled; }
        set { m_Outline.enabled = value; }
    }

    public virtual void Move(Vector3 _destination)
    {

    }

    public virtual void Attack(Selectable _target)
    {

    }

    public virtual void OpenShop()
    {

    }
}
