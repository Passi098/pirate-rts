﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BuildingRegistry : ScriptableObject //Done by: Pascal
{
    public enum EIsleThemes { Wild, Neutral, British }

    public GameObject MainBuilding;

    public GameObject ShipShop;

    public GameObject WeaponShop;
}
