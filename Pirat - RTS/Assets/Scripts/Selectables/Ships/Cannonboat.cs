﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonboat : ShipBehaviour //Lukas
{
    [SerializeField]
    private static float m_Speed = 15f;
    [SerializeField]
    private static float m_Rotation = 15f;
    [SerializeField]
    private float m_MaxAngleToTarget = 10f;
    [SerializeField]
    private int m_MaxHP = 250;
    [SerializeField]
    private int m_MaxCrew = 40;
    [SerializeField]
    private int m_Dmg = 5;
    [SerializeField]
    private GameRegistry.EFractions m_Fraction;

    public override GameRegistry.EFractions Fraction { get { return m_Fraction; } }
    protected override int m_CannonBallDmg { get { return m_Dmg; } }
    protected override int MaxHp { get { return m_MaxHP; } }
    protected override float MaxSpeed { get { return m_Speed; } }
    protected override float MaxRotationSpeed { get { return m_Rotation; } }
    protected override float MaxAngleToTarget { get { return m_MaxAngleToTarget; } }
    protected override bool HasFrontCanons { get { return false; } }
    protected override bool HasSideCanons { get { return true; } }
}
