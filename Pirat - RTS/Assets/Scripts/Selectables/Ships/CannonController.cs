﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CannonController : MonoBehaviour //Lukas
{
    [SerializeField]
    private float m_CannonFireDelay = .25f;
    [SerializeField]
    private float m_FireDelayRandomizerMin = .5f;
    [SerializeField]
    private float m_FireDelayRandomizerMax = 1.5f;
    [SerializeField]
    private float m_CannonCooldown = 15f;
    [SerializeField]
    private GameObject m_CannonBallPrefab;

    private AudioSource m_AudioSource;

    public bool m_LeftSideLoaded { get; private set; } = true;
    public bool m_RightSideLoaded { get; private set; } = true;
    public bool m_FrontSideLoaded { get; private set; } = true;

    [SerializeField]
    public ParticleSystem[] m_CannonShotsRight = new ParticleSystem[default];
    [SerializeField]
    public ParticleSystem[] m_CannonShotsLeft = new ParticleSystem[default];
    [SerializeField]
    public ParticleSystem[] m_CannonShotsFront = new ParticleSystem[default];

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    #region CannonFireCoroutines
    /// <summary>
    /// Fires FX-effect of right broadside, with a slightly randomized delay between each canon and calls FireCanon Method
    /// </summary>
    /// <returns></returns>
    private IEnumerator RightCannonFire()
    {
        if (m_RightSideLoaded)//make sure side is ready
        {
            m_RightSideLoaded = false;
            for (int i = 0; i < m_CannonShotsRight.Length; i++)//iterate through canons and play each effect
            {
                m_CannonShotsRight[i].Play();
                m_AudioSource.Stop();
                m_AudioSource.Play();
                FireCannon(m_CannonShotsRight[i].transform);
                yield return new WaitForSeconds(Random.Range(m_CannonFireDelay * m_FireDelayRandomizerMin, m_CannonFireDelay * m_FireDelayRandomizerMax));
            }
            yield return new WaitForSeconds(m_CannonCooldown);//assure cooldown
            m_RightSideLoaded = true;
        }
    }

    /// <summary>
    /// Fires FX-effect of left broadside, with a slightly randomized delay between each canon and calls FireCanon Method
    /// </summary>
    /// <returns></returns>
    private IEnumerator LeftCannonFire()
    {
        if (m_LeftSideLoaded)//make sure side is ready
        {
            m_LeftSideLoaded = false;
            for (int i = 0; i < m_CannonShotsLeft.Length; i++)//iterate through canons and play each effect
            {
                m_CannonShotsLeft[i].Play();
                m_AudioSource.Stop();
                m_AudioSource.Play();
                FireCannon(m_CannonShotsLeft[i].transform);
                yield return new WaitForSeconds(Random.Range(m_CannonFireDelay * m_FireDelayRandomizerMin, m_CannonFireDelay * m_FireDelayRandomizerMax));
            }
            yield return new WaitForSeconds(m_CannonCooldown);//assure cooldown
            m_LeftSideLoaded = true;
        }
    }

    /// <summary>
    /// Fires FX-effect of FrontCanons, with a slightly randomized delay between each canon and calls FireCanon Method
    /// </summary>
    /// <returns></returns>
    private IEnumerator FrontCannonFire()
    {
        if (m_FrontSideLoaded)//make sure side is ready
        {
            m_FrontSideLoaded = false;
            for (int i = 0; i < m_CannonShotsFront.Length; i++)//iterate through canons and play each effect
            {
                m_CannonShotsFront[i].Play();
                m_AudioSource.Stop();
                m_AudioSource.Play();
                FireCannon(m_CannonShotsFront[i].transform);
                yield return new WaitForSeconds(Random.Range(m_CannonFireDelay * m_FireDelayRandomizerMin, m_CannonFireDelay * m_FireDelayRandomizerMax));
            }
            yield return new WaitForSeconds(m_CannonCooldown);//assure cooldown
            m_FrontSideLoaded = true;
        }

    }
    #endregion CannonFireCoroutines

    /// <summary>
    /// Start or stop the relative coroutine
    /// </summary>
    public void FireRightBrightSide(bool _start)
    {
        if (_start == true)
        {
            StartCoroutine(RightCannonFire());
        }
        else
        {
            Debug.Log("Coroutine gestoppt");
            StopCoroutine(RightCannonFire());
        }
    }

    /// <summary>
    /// Start or stop the relative coroutine
    /// </summary>
    public void FireLeftBrightSide(bool _start)
    {
        if (_start == true)
        {
            StartCoroutine(LeftCannonFire());
        }
        else
        {
            StopCoroutine(LeftCannonFire());
        }
    }

    /// <summary>
    /// Start or stop the relative coroutine
    /// </summary>
    public void FireFrontCannons(bool _start)
    {
        if (_start == true)
        {
            StartCoroutine(FrontCannonFire());
        }
        else
        {
            StopCoroutine(FrontCannonFire());
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="_firePoint">starting point of the shot</param>
    private void FireCannon(Transform _firePoint)
    {
        GameObject cannonBall = Instantiate(m_CannonBallPrefab, _firePoint.position, Quaternion.identity); //Instantiate the bullet
        StartCoroutine(cannonBall.GetComponent<CannonBallBehaviour>().CannonBallMovement(_firePoint)); //call the CannonBallBehaviour coroutine to start movement
    }

}
