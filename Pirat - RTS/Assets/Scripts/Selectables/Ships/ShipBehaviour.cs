﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(CannonController))]
public abstract class ShipBehaviour : Selectable //Lukas
{
    public enum EShiptype
    {
        Flagship,
        Galleon,
        Cannonboat,
        Shallop
    }

    //--------------------------------------------------------------------
    public enum EBehaviour
    {
        Move,
        Attack
    }
    private EBehaviour m_CurrentBehaviour = EBehaviour.Move;
    //---------------------------------------------------------------------

    public NavMeshAgent m_ShipNavMesh;
    protected CannonController m_CannonController;
    protected ShipGUI m_ShipGUI;
    public FightManager m_FightManager { get; private set; }

    protected string m_Shipname;

    protected bool m_IsAlive { get; private set; }

    protected int m_HP;
    protected int m_Crew;
    protected int m_AmmoStandard;
    protected int m_AmmoChain;
    protected int m_AmmoSplinter;

    protected EShiptype m_Shiptype;

    //---------------------------------------------------------------------
    private IEnumerator m_CurrentCoroutine;
    //---------------------------------------------------------------------

    [SerializeField]
    private GameObject m_GUIObject;

    private Vector3 Velocity { get { return m_ShipNavMesh.velocity; } set { m_ShipNavMesh.velocity = value; } }

    protected virtual int m_CannonBallDmg { get { return 3; } }

    //virtual properties to override Max-... for inheriting classes 
    public override GameRegistry.EFractions Fraction { get { return 0; } }
    protected virtual int MaxHp { get { return 1; } } //default value 1 to make sure not to divide with 0 somewhere else
    protected virtual int MaxCrew { get { return 1; } }
    protected virtual int MaxAmmoStandard { get { return 1; } }
    protected virtual int MaxAmmoChain { get { return 1; } }
    protected virtual int MaxAmmoSplinter { get { return 1; } }
    protected virtual float MaxAcceleration { get { return 1; } }
    protected virtual float MaxSpeed { get { return 1; } }
    protected virtual float MaxAngleToTarget { get { return 10f; } }
    protected virtual float MaxRotationSpeed { get { return 10f; } }
    protected virtual bool HasFrontCanons { get { return false; } }
    protected virtual bool HasSideCanons { get { return false; } }
    protected static float SqrCannonRange { get { return 75000; } }

    public delegate void OnDestroyCallback(ShipBehaviour _thisSB, GameRegistry.EFractions _destroyedBy);

    public System.Action m_BattleBeginCallback;
    public System.Action m_BattleEndCallback;
    public OnDestroyCallback m_OnDestroyCallback;

    /// <summary>
    /// Dependent on the remaining Crew, reduce efficiency of the ship
    /// - acceleration
    /// - speed
    /// </summary>
    protected virtual int Crew
    {
        get { return m_Crew; }
        set //set standard values
        {
            if (value < 0)
            {
                value = 0;
            }
            else if (value > MaxCrew)
            {
                value = MaxCrew;
            }
            m_Crew = value;
            float percentagedCrew = m_Crew / MaxCrew; //percentage of the remaining crew
            m_ShipNavMesh.acceleration = MaxAcceleration * percentagedCrew;
            m_ShipNavMesh.speed = MaxSpeed * percentagedCrew;
        }
    }

    #region SailController

    [SerializeField]
    private GameObject[] m_OpenSails = new GameObject[default];

    [SerializeField]
    private GameObject[] m_CloseSails = new GameObject[default];

    private bool m_SailsOpen = false;

    private bool OpenSail
    {
        get
        {
            return m_SailsOpen;
        }
        set
        {
            if (value == m_SailsOpen)
            {
                return;
            }
            m_SailsOpen = value;
            for (int i = 0; i < m_OpenSails.Length; i++)
            {
                m_OpenSails[i].SetActive(m_SailsOpen);
            }
            for (int i = 0; i < m_CloseSails.Length; i++)
            {
                m_CloseSails[i].SetActive(!m_SailsOpen);
            }
        }
    }


    #endregion SailController

    //---------------------------------------------------------------------------
    #region Behaviour Change
    public void StartAttackBehaviour()
    {
        if (m_CurrentBehaviour == EBehaviour.Attack)
        {
            return;
        }
        if (m_CurrentCoroutine != null)
        {
            StopCoroutine(m_CurrentCoroutine);
        }
        m_ShipNavMesh.isStopped = true;
        m_ShipNavMesh.enabled = false;

        m_CurrentCoroutine = UpdateBoidBehaviour();
        StartCoroutine(m_CurrentCoroutine);

        m_CurrentBehaviour = EBehaviour.Attack;
    }

    public void StartMoveBehaviour(Vector3 _commandedPos)
    {
        if (m_CurrentCoroutine != null)
        {
            StopCoroutine(m_CurrentCoroutine);
        }

        if (m_CurrentBehaviour == EBehaviour.Attack)
        {
            m_Target = null;
            if (m_FightManager != null)
            {
                m_FightManager.m_FightingShips.Remove(this);
                m_FightManager = null;
            }

            m_ShipNavMesh.enabled = true;
            m_ShipNavMesh.isStopped = false;
        }

        m_CurrentCoroutine = MovementCoroutine(_commandedPos);
        StartCoroutine(m_CurrentCoroutine);

        m_CurrentBehaviour = EBehaviour.Move;
    }

    #endregion Behaviour Change
    //---------------------------------------------------------------------------

    /// <summary>
    /// Awake of the Selectable script
    /// </summary>
    protected override void SelectableAwake()
    {
        //do some set ups
        m_ShipNavMesh = GetComponent<NavMeshAgent>();
        m_ShipNavMesh.autoBraking = true;
        m_ShipNavMesh.autoRepath = true;
        m_ShipNavMesh.autoTraverseOffMeshLink = true;
        //TODO: Angular Speed
        //TODO: BaseOffSet
        //TODO: Height
        //TODO: Radius
        //m_ShipNavMesh.stoppingDistance = (MaxSpeed / MaxAcceleration) * MaxSpeed; //to calc distance for starting to brake       
        m_ShipNavMesh.updateRotation = true;


        m_BoidList.Add(this); //add this as a boid and shipbehaviour for easier access
        m_CannonController = GetComponent<CannonController>();
        m_ShipGUI = GetComponent<ShipGUI>();

        m_HP = MaxHp;
        m_Crew = MaxCrew;

        if(Fraction == GameRegistry.EFractions.Pirates)
        {
            GameRegistry.AddShipsToPlayerShips(this);
        }
    }

    #region Movement & Angles

    /// <summary>
    /// Give in a commanded position to move shipt to destination
    /// </summary>
    /// <param name="_commandedPos">destination</param>
    public void MoveToCommandedPosition(Vector3 _commandedPos)
    {
        StartMoveBehaviour(_commandedPos);
    }

    /// <summary>
    /// Move is called from Selecter and calls MoveToCommandedPosition
    /// </summary>
    /// <param name="_destination"></param>
    public override void Move(Vector3 _destination)
    {
        MoveToCommandedPosition(_destination);
    }

    /// <summary>
    /// Rotates ship into the right angle to start its movement towards destination
    /// </summary>
    /// <param name="_commandedPos">destination</param>
    /// <returns></returns>
    private IEnumerator MovementCoroutine(Vector3 _commandedPos)
    {
        m_ShipNavMesh.isStopped = true;

        if (_commandedPos != transform.position)
        {

            //checking angle to target and control rotation
            float angleToTarget = SignedAngle(transform.forward, _commandedPos - transform.position, Vector3.up);
            if (Mathf.Abs(angleToTarget) > MaxAngleToTarget)
            {
                do
                {
                    float rotateTime = (Mathf.Abs(angleToTarget) - MaxAngleToTarget) / MaxRotationSpeed; //time needed to fully rotate

                    float rotationDirection = angleToTarget / Mathf.Abs(angleToTarget); //to check in what direction to rotate (which one is faster)

                    float frames = rotateTime * 50;

                    do //rotate ship to target position
                    {
                        transform.Rotate(0, MaxRotationSpeed * rotationDirection * 0.02f, 0);
                        OpenSail = false;
                        frames--;
                        yield return new WaitForSeconds(0.02f);
                    } while (frames > 0);

                    angleToTarget = SignedAngle(transform.forward, _commandedPos - transform.position, Vector3.up); //refresh angleToTargetPos

                } while (Mathf.Abs(angleToTarget) >= MaxAngleToTarget);//check again
            }
            m_ShipNavMesh.isStopped = false;
            m_ShipNavMesh.SetDestination(_commandedPos); //start actually driving/swimming (whatever) to destination
            m_ShipNavMesh.speed = MaxSpeed;

            OpenSail = true; //set sails
            float velocity;
            float tolerance = .75f; //tolerance for slowing down/ deactivating sail- things

            //Sail Controlling during movement
            do  //to deactivate Sails when slowing down
            {
                velocity = m_ShipNavMesh.velocity.magnitude;
                yield return new WaitForSeconds(1f);//wait a second, check again
            } while (m_ShipNavMesh.velocity.magnitude >= (velocity - tolerance)); //NOTE: remaining distance could be used
            OpenSail = false;
            do //to activate Sails when slowing down
            {
                velocity = m_ShipNavMesh.velocity.magnitude;
                yield return new WaitForSeconds(1f);//wait a second, check again
            } while (m_ShipNavMesh.velocity.magnitude <= (velocity - tolerance));
            OpenSail = true;
        }


    }

    /// <summary>
    /// local function to check for angle
    /// </summary>
    /// <param name="from">look dir</param>
    /// <param name="to">target pos</param>
    /// <param name="normal">axis</param>
    /// <returns></returns>
    private float SignedAngle(Vector3 from, Vector3 to, Vector3 normal)
    {
        // angle in [0,180]
        return Vector3.Angle(from, to) * Mathf.Sign(Vector3.Dot(normal, Vector3.Cross(from, to)));
    }

    #endregion Movement & Angles

    #region Attack & Dmg

    /// <summary>
    /// Attack an enemy target and call fight-boid-behaviour
    /// </summary>
    /// <param name="_target"></param>
    public override void Attack(Selectable _target)
    {
        m_FightManager = new FightManager();
        m_Target = _target.Boid;
        m_FightManager.m_FightingShips.Add(this);
        m_FightManager.m_FightingShips.Add(m_Target);  //add all participants to m_FightingShips-List

        m_FightManager.CalcFightCenter();
        StartAttackBehaviour();
        m_Target.m_FightManager = m_FightManager;
        m_Target.m_Target = this;
        m_Target.StartAttackBehaviour();
    }

    /// <summary>
    /// Get damage and get destroyed, if hp < 0
    /// </summary>
    /// <param name="_dmg"></param>
    public bool GetDamage(int _dmg, GameRegistry.EFractions _dmgDealer)
    {
        m_HP -= _dmg;
        m_ShipGUI.HP -= ((float)_dmg / (float)MaxHp); // make sure to show the visuals (lowering the Hp ring)
        //Debug.Log("Aua: " + _dmg + " MaxHP: " + MaxHp + " HP: " + m_HP + "---" + ((float)_dmg / (float)MaxHp) + "%");
        if (m_HP <= 0)
        {
            if (m_OnDestroyCallback != null)
            {
                m_OnDestroyCallback(this, _dmgDealer);
                m_OnDestroyCallback = null;
            }

            if (m_FightManager != null)
            {
                m_FightManager.m_FightingShips.Remove(this);
            }
            StartCoroutine(Sinking());
            return true;
        }
        return false;
    }

    private IEnumerator Sinking()
    {
        bool isSunken = false;
        float angle = Random.Range(0.025f, .125f);
        float sinkPerFrame = Random.Range(3.25f, 5.25f);

        //disable GUI and cannons
        m_GUIObject.SetActive(false);
        FireCannons(false);

        while (isSunken == false)
        {
            //the sinking "animation"
            transform.Rotate(Vector3.forward, angle);
            transform.Rotate(Vector3.right, angle);
            transform.position -= new Vector3(0, sinkPerFrame * Time.deltaTime, 0);


            yield return null;
            if (transform.position.y <= -20f)
            {
                isSunken = true;
                Destroy(this.gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        if (m_CurrentCoroutine != null)
        {
            m_IsFighting = false;
            StopCoroutine(m_CurrentCoroutine);
        }
        if (m_FightManager != null)
        {
            m_FightManager.m_FightingShips.Remove(this);
        }
        if(Fraction == GameRegistry.EFractions.Pirates)
        {
            GameRegistry.RemoveShipToPlayerShips(this);
        }
    }

    /// <summary>
    /// calculate the damage, with a little random factor
    /// </summary>
    /// <param name="_noc">number of cannons</param>
    private void CalcDamage(int _noc)
    {
        int dmg = 0;
        dmg = (int)(_noc * Random.Range(.5f, 1.5f) * m_CannonBallDmg);
        if (m_Target.GetDamage(dmg, this.Fraction))
        {
            m_Target = null;
            SelectNewTarget();
            if (m_Target == null && m_CurrentCoroutine != null) //if no target available
            {
                m_IsFighting = false;
                StopCoroutine(m_CurrentCoroutine); //stop boid behaviour

                BritishKI bki = GetComponent<BritishKI>();
                if (bki != null) //check for british ki component
                {
                    bki.StartPatroling(); //start patroling
                }
            }
        }
    }

    /// <summary>
    /// selcting a new target out of target list
    /// </summary>
    private void SelectNewTarget()
    {
        for (int i = 0; i < m_FightManager.m_FightingShips.Count; i++)
        {
            if (m_FightManager.m_FightingShips[i].Fraction != this.Fraction)
            {
                m_Target = m_FightManager.m_FightingShips[i];
                return;
            }
        }
    }

    /// <summary>
    /// check targets angle and fire given side
    /// </summary>
    private void FireCannons(bool _start)
    {
        if (m_AngleShipToTarget <= -85 && m_AngleShipToTarget >= -95 && m_CannonController.m_LeftSideLoaded == true)
        {
            m_CannonController.FireLeftBrightSide(_start);
            CalcDamage(m_CannonController.m_CannonShotsLeft.Length);
        }
        else if (m_AngleShipToTarget >= 85 && m_AngleShipToTarget <= 95 && m_CannonController.m_RightSideLoaded == true)
        {
            m_CannonController.FireRightBrightSide(_start);
            CalcDamage(m_CannonController.m_CannonShotsRight.Length);
        }
        else if (m_AngleShipToTarget >= -5 && m_AngleShipToTarget <= 5 && m_CannonController.m_FrontSideLoaded == true)
        {
            m_CannonController.FireFrontCannons(_start);
            CalcDamage(m_CannonController.m_CannonShotsFront.Length);
        }
    }

    #endregion Attack & Dmg

    #region Boidbehaviour

    private static Vector2 m_SeperationStorage = Vector2.zero;
    private static Vector2 m_AlignmentStorage = Vector2.zero;
    private static Vector2 m_CohesionStorage = Vector2.zero;
    private Vector2 m_Placeholder2 = Vector2.zero;
    private Vector3 m_PlaceHolder = Vector3.zero;
    private static float m_SqrSeperationRadius = Mathf.Pow(75f, 2);
    private static float m_AngleShipToTarget;
    private static float m_AngleTargetToShip;
    private static float m_AbsAngleShipToTarget;
    private static float m_AbsAngleTargetToShip;
    private static float m_CombinedAngle;
    private static float m_CannonAngle;
    private static float m_AgilityMultiplier = 0.02f;
    private static int m_Counter = 0;
    private bool m_IsFighting = false;
    private Vector3 m_Velocity = Vector3.zero;

    private ShipBehaviour m_Target;

    private List<ShipBehaviour> m_BoidList = new List<ShipBehaviour>();

    public override ShipBehaviour Boid { get { return this; } }

    /// <summary>
    /// Update behaviour of the boid, while fighting
    /// </summary>
    /// <returns></returns>
    private IEnumerator UpdateBoidBehaviour()
    {
        m_IsFighting = true;
        if (m_BattleBeginCallback != null)
        {
            m_BattleBeginCallback();
        }
        m_Velocity = Velocity;
        m_ShipNavMesh.enabled = false; //disable NavMeshAgent to start boid behaviour 
        while (m_Target != null && m_IsFighting)
        {
            while (m_Target != null && m_IsFighting) //calc velocity every 10th frame 
            {
                Boidbehaviour();
                transform.position += transform.forward * MaxSpeed * Time.deltaTime;
                if (m_Target != null)
                {
                    if ((transform.position - m_Target.transform.position).sqrMagnitude <= SqrCannonRange) //when in range to target, start shooting from right direction
                    {
                        FireCannons(true);
                    }
                }
                yield return null;
            }
            for (int i = 0; i < m_FightManager.m_FightingShips.Count; i++)
            {
                if (m_FightManager.m_FightingShips[i].Fraction != this.Fraction)
                {
                    m_Target = m_FightManager.m_FightingShips[i];
                }
            }
        }
        if (m_BattleEndCallback != null)
        {
            m_ShipNavMesh.enabled = true;
            m_ShipNavMesh.isStopped = false;
            m_BattleEndCallback();
        }
    }

    /// <summary>
    /// the actual boid behaviour calculated using CalcSeperation, CalcCohesion and CalcAlignment
    /// </summary>
    private void Boidbehaviour()
    {
        if (m_Target == null)
        {
            return;
        }
        //prioritization of seperation, cohesion and alignment
        if (CalcSeperation())
        {
            m_PlaceHolder.x = m_SeperationStorage.x;
            m_PlaceHolder.y = 0f;
            m_PlaceHolder.z = m_SeperationStorage.y;
            if (CalcCohesion())
            {
                m_PlaceHolder.x += m_CohesionStorage.x;
                m_PlaceHolder.z += m_CohesionStorage.y;
            }
            else
            {
                CalcAlignment();
                m_PlaceHolder.x += m_AlignmentStorage.x;
                m_PlaceHolder.z += m_AlignmentStorage.y;
            }
        }
        else
        {
            m_PlaceHolder.x = m_SeperationStorage.x;
            m_PlaceHolder.y = 0f;
            m_PlaceHolder.z = m_SeperationStorage.y;
        }
        m_Velocity += (m_PlaceHolder / 2) * MaxSpeed;
        if (m_Velocity.sqrMagnitude > Mathf.Pow(MaxSpeed, 2))
        {
            m_Velocity = m_Velocity.normalized * MaxSpeed;
        }

        float rotationDirection = SignedAngle(transform.forward, m_Velocity, Vector3.up);

        transform.Rotate(0, MaxRotationSpeed * Mathf.Clamp(rotationDirection, -1, 1) * m_AgilityMultiplier, 0);

        if (Mathf.Abs(rotationDirection) < MaxAngleToTarget)
        {
            transform.position += m_Velocity * Time.deltaTime;
        }
    }

    /// <summary>
    /// dodge other ships, or islands
    /// </summary>
    bool CalcSeperation()
    {
        m_SeperationStorage = Vector2.zero;
        m_Counter = 0;
        for (int i = 0; i < m_FightManager.m_FightingShips.Count; i++)
        {
            if (m_FightManager.m_FightingShips[i] != this && m_FightManager != null)
            {
                m_PlaceHolder = transform.position - m_FightManager.m_FightingShips[i].transform.position;
                if (m_PlaceHolder.sqrMagnitude <= m_SqrSeperationRadius)
                {
                    m_Counter++;
                    m_SeperationStorage += new Vector2(m_PlaceHolder.x, m_PlaceHolder.z).normalized;
                }
            }
        }

        #region AvoidIsles
        Vector2 pos = new Vector2(transform.position.x, transform.position.z);
        for (int i = 0; i < m_FightManager.m_Isles.Count; i++)
        {
            m_Placeholder2 = pos - m_FightManager.m_Isles[i].m_IsleCenter;
            if ((m_PlaceHolder).sqrMagnitude <= m_FightManager.m_Isles[i].m_SqrIsleRadius)
            {
                m_SeperationStorage += m_Placeholder2 * 10;
                m_Counter++;
            }
        }

        #endregion AvoidIsles

        if (m_Counter == 0)
        {
            return true;
        }
        m_SeperationStorage /= m_Counter;
        m_SeperationStorage = m_SeperationStorage.normalized * Time.deltaTime;
        return false;
    }

    /// <summary>
    /// aim with canons for max dmg
    /// </summary>
    void CalcAlignment()
    {
        m_AlignmentStorage = Vector2.zero;
        if (m_Target == null)
        {
            return;
        }

        if (!HasSideCanons)
        {
            AimFrontCannons();
            return;
        }

        m_AngleShipToTarget = SignedAngle(transform.forward, m_Target.transform.position - transform.position, Vector3.up);

        if (!HasFrontCanons)
        {
            TryAimSideCannons();
        }

        m_AngleTargetToShip = SignedAngle(m_Target.transform.forward, transform.position - m_Target.transform.position, Vector3.up);
        m_CombinedAngle = m_AngleTargetToShip + m_AngleShipToTarget;
        m_AbsAngleShipToTarget = Mathf.Abs(m_AngleShipToTarget);
        m_AbsAngleTargetToShip = Mathf.Abs(m_AngleTargetToShip);

        //TryAimSideCannons();
        //return;

        if (m_AngleShipToTarget <= 45)
        {
            if (m_AngleTargetToShip <= 135)
            {
                //cross us
                AimSideCannons();
            }
            else
            {
                //away from us
                AimFrontCannons();
            }
        }

        else
        {
            TryAimSideCannons();
        }

        void AimFrontCannons()
        {
            m_AlignmentStorage = (m_Target.transform.position - transform.position).normalized * Time.deltaTime; //move towards it
        }

        void AimSideCannons()
        {
            if (m_AngleShipToTarget > 0f) //if on our right side
            {
                m_CannonAngle = SignedAngle(transform.right, m_Target.transform.position - transform.position, Vector3.up); //calc CannonAngle for broadside
                m_AlignmentStorage = Mathf.Clamp(m_CannonAngle, -1, 1) * new Vector2(transform.right.x, transform.right.z) * Time.deltaTime; //turn by
            }
            else
            {
                m_CannonAngle = SignedAngle(-transform.right, m_Target.transform.position - transform.position, Vector3.up); //calc CannonAngle for broadside
                m_AlignmentStorage = Mathf.Clamp((m_CannonAngle), -1, 1) * new Vector2(transform.right.x, transform.right.z) * Time.deltaTime; //turn by
            }
        }

        void TryAimSideCannons()
        {
            if ((m_Target.transform.position - transform.position).sqrMagnitude <= SqrCannonRange)
            {
                AimSideCannons();
            }
            else
            {
                AimFrontCannons();
            }
        }
    }


    /// <summary>
    /// orientation to center of battle
    /// </summary>
    bool CalcCohesion()
    {
        if ((transform.position - m_FightManager.m_FightCenter3D).sqrMagnitude <= m_FightManager.m_SqrdMaxDistanceToCenter)
        {
            return false;
        }
        m_CohesionStorage = Vector2.zero;
        m_CohesionStorage = (m_FightManager.m_FightCenter - new Vector2(transform.position.x, transform.position.z)).normalized * Time.deltaTime;
        return true;
    }

    #endregion Boidbehaviour

}