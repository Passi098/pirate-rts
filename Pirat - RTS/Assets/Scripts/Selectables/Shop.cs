﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : Selectable //Done by: Pascal
{
    public Isle m_Isle;

    public override GameRegistry.EFractions Fraction
    {
        get { return m_Isle.Fraction; }
    }

    public override GameRegistry.ESelectableType SelectableType
    {
        get { return GameRegistry.ESelectableType.Shop; }
    }

    public void Buy(ShipBehaviour.EShiptype _type, int _price)
    {
        if(Fraction != GameRegistry.EFractions.Pirates || GameRegistry.m_PlayerGold < _price)
        {
            return;
        }
        m_Isle.Spawn(_type, GameRegistry.EFractions.Pirates);
        GameRegistry.m_PlayerGold -= _price;
    }
}
