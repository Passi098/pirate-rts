﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshObstacle))]
public class Isle : MonoBehaviour //Done by: Pascal
{
    private GameRegistry.EFractions m_Fraction;

    public static List<Isle> m_Isles = new List<Isle>();


    private const float AUTO_GARNISION_RANGE = 50;
    private const float SQR_AUTO_GARNISION_RANGE = AUTO_GARNISION_RANGE * AUTO_GARNISION_RANGE;

    public ShipRegistry m_ShipRegistry;
    public Vector2 m_IsleCenter;
    public float m_SqrIsleRadius;
    public float m_IsleRadius;

    [SerializeField]
    public NavMeshObstacle m_NavMeshObstacle;

    public int m_GarnisionStrength;
    public int m_PatrolStrength;

    private const int FLAGSHIP_STRENGTH = 8;
    private const int GALLEON_STRENGTH = 4;
    private const int CANNONBOAT_STRENGTH = 2;
    private const int SHALLOP_STRENGTH = 1;

    public List<ShipBehaviour> m_Garnision = new List<ShipBehaviour>();

    public List<ShipBehaviour> m_Patrol;

    public float m_Loot { get; private set; } = 0;

    public float m_LootRising = 0;

    public float SqrIsleRadius
    {
        set
        {
            m_SqrIsleRadius = value;
            m_IsleRadius = Mathf.Sqrt(m_SqrIsleRadius);
            m_NavMeshObstacle.radius = m_IsleRadius;
            //m_NavMeshObstacle.center = m_IsleCenter;
            m_NavMeshObstacle.center = new Vector3(m_IsleCenter.x - transform.position.x, 0 - transform.position.y , m_IsleCenter.y - transform.position.z);
        }
    }

    public GameRegistry.EFractions Fraction
    {
        get { return m_Fraction; }
        set
        {
            if(value == GameRegistry.EFractions.Pirates)
            {
                GameRegistry.m_PlayerGold += m_Loot;
            }
            m_Loot = 0;
            //Reset ro Default
            switch (m_Fraction)
            {
                case GameRegistry.EFractions.Pirates:
                    {
                        GameRegistry.RemoveIsleFromPlayerIsles(this);
                    }
                    break;
                case GameRegistry.EFractions.British:
                    {
                        for (int i = 0; i < m_Garnision.Count; i++)
                        {
                            Destroy(m_Garnision[i].gameObject);
                        }
                        for (int i = 0; i < m_Patrol.Count; i++)
                        {
                            Destroy(m_Patrol[i].gameObject);
                        }
                        m_Garnision.Clear();
                        m_Patrol.Clear();
                        m_Patrol = null;
                    }
                    break;
                case GameRegistry.EFractions.Neutral:
                    {
                        for (int i = 0; i < m_Garnision.Count; i++)
                        {
                            Destroy(m_Garnision[i].gameObject);
                        }
                        m_Garnision.Clear();
                    }
                    break;
            }

            m_Fraction = value;

            //Setup Isle
            switch (value)
            {
                case GameRegistry.EFractions.Pirates:
                    {
                        GameRegistry.AddIsleToPlayerIsles(this);
                        if (GameRegistry.m_PlayerShips.Count > 0)
                        {
                            for (int i = 0; i < GameRegistry.m_PlayerShips.Count; i++)
                            {
                                if ((GameRegistry.m_PlayerShips[i].transform.position - transform.position).sqrMagnitude <= SQR_AUTO_GARNISION_RANGE)
                                {
                                    GameRegistry.m_PlayerShips[i].m_OnDestroyCallback = GarnisionDestroyedCallback;
                                    GameRegistry.m_PlayerShips[i].MoveToCommandedPosition(transform.position);
                                }
                            }
                            if (m_Garnision.Count <= 0)
                            {
                                float distance;
                                float nearestDistance = (GameRegistry.m_PlayerShips[0].transform.position - transform.position).sqrMagnitude;
                                ShipBehaviour nearestShip = GameRegistry.m_PlayerShips[0];
                                for (int i = 1; i < GameRegistry.m_PlayerShips.Count; i++)
                                {
                                    distance = (GameRegistry.m_PlayerShips[i].transform.position - transform.position).sqrMagnitude;
                                    if (distance < nearestDistance)
                                    {
                                        nearestDistance = distance;
                                        nearestShip = GameRegistry.m_PlayerShips[i];
                                    }
                                }
                                nearestShip.m_OnDestroyCallback = GarnisionDestroyedCallback;
                                nearestShip.MoveToCommandedPosition(transform.position);
                            }
                        }
                    }
                    break;
                case GameRegistry.EFractions.Neutral:
                    {
                        SpawnGarnision();
                    }
                    break;
                case GameRegistry.EFractions.British:
                    {
                        SpawnGarnision();
                        SpawnPatrol();
                    }
                    break;
            }
        }
    }

    public ShipBehaviour Spawn(ShipBehaviour.EShiptype _type, GameRegistry.EFractions _fraction)
    {
        Vector2 spawnPos2 = Random.insideUnitCircle.normalized * m_IsleRadius * 1.5f;
        Vector3 spawnPos = new Vector3(spawnPos2.x + m_IsleCenter.x, -0.5f, spawnPos2.y + m_IsleCenter.y);
        switch (_fraction)
        {
            case GameRegistry.EFractions.Pirates:
                {
                    switch (_type)
                    {
                        case ShipBehaviour.EShiptype.Flagship:
                            {
                                return Instantiate(m_ShipRegistry.Flagship, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Galleon:
                            {
                                return Instantiate(m_ShipRegistry.Galleon, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Cannonboat:
                            {
                                return Instantiate(m_ShipRegistry.Cannonboat, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Shallop:
                            {
                                return Instantiate(m_ShipRegistry.Shallop, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                    }
                }
                break;
            case GameRegistry.EFractions.Neutral:
                {
                    switch (_type)
                    {
                        case ShipBehaviour.EShiptype.Flagship:
                            {
                                return Instantiate(m_ShipRegistry.NeutralFlagship, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Galleon:
                            {
                                return Instantiate(m_ShipRegistry.NeutralGalleon, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Cannonboat:
                            {
                                return Instantiate(m_ShipRegistry.NeutralCannonboat, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Shallop:
                            {
                                return Instantiate(m_ShipRegistry.NeutralShallop, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                    }
                }
                break;
            case GameRegistry.EFractions.British:
                {
                    switch (_type)
                    {
                        case ShipBehaviour.EShiptype.Flagship:
                            {
                                return Instantiate(m_ShipRegistry.BritishFlagship, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Galleon:
                            {
                                return Instantiate(m_ShipRegistry.BritishGalleon, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Cannonboat:
                            {
                                return Instantiate(m_ShipRegistry.BritishCannonboat, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                        case ShipBehaviour.EShiptype.Shallop:
                            {
                                return Instantiate(m_ShipRegistry.BritishShallop, spawnPos, default, null).GetComponent<ShipBehaviour>();
                            }
                    }
                }
                break;
        }
        return null;
    }

    private void SpawnGarnision()
    {
        int leftStrength = m_GarnisionStrength;
        ShipBehaviour ship;
        BritishKI ki;
        //while (leftStrength >= FLAGSHIP_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 20))
        //{
        //    ship = Spawn(ShipBehaviour.EShiptype.Flagship, Fraction);
        //    if (Fraction == GameRegistry.EFractions.British)
        //    {
        //        ki = ship.gameObject.GetComponent<BritishKI>();
        //        ki.HomeIsland = this;
        //    }
        //    ship.m_OnDestroyCallback = GarnisionDestroyedCallback;
        //    leftStrength -= FLAGSHIP_STRENGTH;
        //}
        while (leftStrength >= GALLEON_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 8))
        {
            ship = Spawn(ShipBehaviour.EShiptype.Galleon, Fraction);
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
            }
            ship.m_OnDestroyCallback = GarnisionDestroyedCallback;
            leftStrength -= GALLEON_STRENGTH;
        }
        while (leftStrength >= CANNONBOAT_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 4))
        {
            ship = Spawn(ShipBehaviour.EShiptype.Cannonboat, Fraction);
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
            }
            ship.m_OnDestroyCallback = GarnisionDestroyedCallback;
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
            }
            leftStrength -= CANNONBOAT_STRENGTH;
        }
        while (leftStrength >= SHALLOP_STRENGTH)
        {
            ship = Spawn(ShipBehaviour.EShiptype.Shallop, Fraction);
            ship.m_OnDestroyCallback = GarnisionDestroyedCallback;
            leftStrength -= SHALLOP_STRENGTH;
        }
    }

    private void SpawnPatrol()
    {
        int leftStrength = m_PatrolStrength;
        ShipBehaviour ship;
        BritishKI ki;
        //while (leftStrength >= FLAGSHIP_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 14))
        //{
        //    ship = Spawn(ShipBehaviour.EShiptype.Flagship, Fraction);
        //    if (Fraction == GameRegistry.EFractions.British)
        //    {
        //        ki = ship.gameObject.GetComponent<BritishKI>();
        //        ki.HomeIsland = this;
        //        ki.StartPatroling();
        //    }
        //    ship.m_OnDestroyCallback = PatrolDestroyedCallback;
        //    leftStrength -= FLAGSHIP_STRENGTH;
        //}
        while (leftStrength >= GALLEON_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 6))
        {
            ship = Spawn(ShipBehaviour.EShiptype.Galleon, Fraction);
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
                ki.StartPatroling();
            }
            ship.m_OnDestroyCallback = PatrolDestroyedCallback;
            leftStrength -= GALLEON_STRENGTH;
        }
        while (leftStrength >= CANNONBOAT_STRENGTH && (Random.Range(0, 101) > 25 || leftStrength > 2))
        {
            ship = Spawn(ShipBehaviour.EShiptype.Cannonboat, Fraction);
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
                ki.StartPatroling();
            }
            ship.m_OnDestroyCallback = PatrolDestroyedCallback;
            leftStrength -= CANNONBOAT_STRENGTH;
        }
        while (leftStrength >= SHALLOP_STRENGTH)
        {
            ship = Spawn(ShipBehaviour.EShiptype.Shallop, Fraction);
            if (Fraction == GameRegistry.EFractions.British)
            {
                ki = ship.gameObject.GetComponent<BritishKI>();
                ki.HomeIsland = this;
                ki.StartPatroling();
            }
            ship.m_OnDestroyCallback = PatrolDestroyedCallback;
            leftStrength -= SHALLOP_STRENGTH;
        }
    }

    private void Awake()
    {
        m_Isles.Add(this);
    }

    private void OnDestroy()
    {
        m_Isles.Remove(this);
    }

    private void Update()
    {
        if (Fraction != GameRegistry.EFractions.Pirates)
        {
            m_Loot += m_LootRising * Time.deltaTime;
        }
        else
        {
            GameRegistry.m_PlayerGold += m_LootRising * Time.deltaTime;
        }
    }

    private void GarnisionDestroyedCallback(ShipBehaviour _ship, GameRegistry.EFractions _destroyedBy)
    {
        m_Garnision.Remove(_ship);
        CheckIfCaptured(_destroyedBy);
    }

    private void PatrolDestroyedCallback(ShipBehaviour _ship, GameRegistry.EFractions _placeholder)
    {
        m_Patrol.Remove(_ship);
    }

    private void CheckIfCaptured(GameRegistry.EFractions _possibleNewOwner)
    {
        if (m_Garnision.Count > 0)
        {
            return;
        }
        Fraction = _possibleNewOwner;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(new Vector3(m_IsleCenter.x, 5, m_IsleCenter.y), Mathf.Sqrt(m_SqrIsleRadius));
    }
}
