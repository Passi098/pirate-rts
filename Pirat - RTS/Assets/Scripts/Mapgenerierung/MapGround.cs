﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MapGround : MonoBehaviour //Done by: Pascal
{
    #region Variables

    private const int CHUNCK_EDGE_SIZE = 256;

    [SerializeField]
    private ShipRegistry m_ShipRegistry;

    [SerializeField]
    GameObject m_ChunkPrefab;

    [SerializeField]
    float m_MapScaleX = 1;

    [SerializeField]
    float m_MapScaleZ = 1;

    [SerializeField]
    float m_MapHeight = 1;

    [SerializeField]
    uint m_MapSizeX = 1;

    [SerializeField]
    uint m_MapSizeZ = 1;

    [SerializeField]
    float m_MapSmoothness = 1;

    [SerializeField]
    int m_MapSeed = 1;

    [SerializeField]
    AnimationCurve m_HeightCurve;

    [Header("Cellular Automata")]
    [SerializeField]
    [Range(0, 100)]
    int m_CaTrueStart = 25;

    [SerializeField]
    int m_CaInitStartGen = 5;

    [SerializeField]
    [Range(0, 8)]
    int m_CaRequierdNeighbours = 4;

    bool[,] m_CAMap;

    [Header("Buildings")]
    [SerializeField]
    private BuildingRegistry m_BritishThemedBuildings;

    [SerializeField]
    private BuildingRegistry m_NeutralThemedBuildings;

    [SerializeField]
    private BuildingRegistry m_WildThemedBuildings;

    [Header("Water")]
    [SerializeField]
    private Material m_WaterMaterial;

    [SerializeField]
    private Color m_DeepWaterColor;

    [SerializeField]
    private Color m_Watercolor;

    [SerializeField]
    private Color m_IsleColor;
    #endregion Variables

    // Start is called before the first frame update
    void Start()
    {
        m_MapSeed = GameRegistry.m_MapSeed;
        float[,] map = GenerateBaseMap();

        #region Define Isles

        List<MapIsle> isles = new List<MapIsle>();
        MapIsle isle;
        //Define Isles
        for (Vector2Int vec = Vector2Int.zero; vec.x < m_MapSizeX; vec.x++)
        {
            for (vec.y = 0; vec.y < m_MapSizeZ; vec.y++)
            {
                isle = DefineIsle(vec);
                if(isle != null)
                {
                    isles.Add(isle);
                }
            }
        }

        Debug.Log("There are " + isles.Count + " isles on the map");

        MapIsle DefineIsle(Vector2Int _vec)
        {
            if (map[_vec.x, _vec.y] != 1)
            {
                return null;
            }
            if(FieldIsIsle(_vec))
            {
                return null;
            }
            MapIsle newMapIsle = new MapIsle();
            SizeIsle(_vec , newMapIsle);
            return newMapIsle;
        }

        void SizeIsle(Vector2Int _vec, MapIsle _isle)
        {
            if(map[_vec.x, _vec.y] != 1)
            {
                return;
            }
            if(_isle.Contains(_vec))
            {
                return;
            }
            _isle.Add(_vec);
            SizeIsle(_vec + Vector2Int.left, _isle);
            SizeIsle(_vec + Vector2Int.down, _isle);
            SizeIsle(_vec + Vector2Int.right, _isle);
            SizeIsle(_vec + Vector2Int.up, _isle);
        }

        bool FieldIsIsle(Vector2Int _vec)
        {
            for (int i = 0; i < isles.Count; i++)
            {
                if(isles[i].Contains(_vec))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion Define Isles
        
        float[,] noisemap = NoiseMapGenerator.GenerateNoisemap(m_MapSizeX, m_MapSizeZ, m_MapSmoothness, m_MapSeed);
        Texture2D texture = new Texture2D((int)m_MapSizeX, (int)m_MapSizeZ);
        texture.name = "Minimap";
        for (int x = 0; x < m_MapSizeX; x++)
        {
            for (int z = 0; z < m_MapSizeZ; z++)
            {
                if(map[x, z] != 1)
                {
                    map[x, z] = Mathf.Clamp((noisemap[x, z] + map[x, z] * 4) / 2, 0, 1);
                    if (map[x, z] < 0.4f)
                    {
                        texture.SetPixel(x, z, m_DeepWaterColor);
                    }
                    else
                    {
                        texture.SetPixel(x, z, m_Watercolor);
                    }
                }
                else
                {
                    texture.SetPixel(x, z, m_IsleColor);
                }

            }
        }
        texture.Apply();
        Minimap.SetImage = Sprite.Create(texture, new Rect(Vector2.zero, new Vector2(m_MapSizeX, m_MapSizeZ)), new Vector2(m_MapSizeX / 2, m_MapSizeZ / 2));

        

        //for (int x = 0; x < m_MapSizeX; x++)
        //{
        //    for (int z = 0; z < m_MapSizeZ; z++)
        //    {
        //        map[x, z] = m_HeightCurve.Evaluate(map[x, z]);
        //    }
        //}

        // Multiplays heightpoints with max height
        for (int x = 0; x < m_MapSizeX; x++)
        {
            for (int z = 0; z < m_MapSizeZ; z++)
            {
                map[x, z] *= m_MapHeight;
            }
        }

        //Generate Mesh
        GenerateMap(map);

        #region Buildings

        Dictionary<BuildingRegistry.EIsleThemes, BuildingRegistry> themes;
        Isle newIsle = null;
        bool playerShipSpawned = false;
        for (int isleID = 0; isleID < isles.Count; isleID++)
        {
            int shopPosIndex = Random.Range(0, isles[isleID].m_Fields.Count - 1);
            int posIndex = Random.Range(0, isles[isleID].m_Fields.Count);
            Vector2Int pos = isles[isleID].m_Fields[posIndex];
            Vector3 vec = new Vector3(pos.x * m_MapScaleX, 5, pos.y * m_MapScaleZ);//transform.position.y + m_MapHeight * 0.75f
            Vector2Int shopPos;
            Vector3 shopVec;

            if (isles[isleID].m_Fields.Count >= 2)
            {
                if (shopPosIndex == posIndex)
                {
                    shopPosIndex++;
                }
                //Debug.Log(isles[isleID].m_Fields.Count + ", " + posIndex + ",  " + shopPosIndex);
                shopPos = isles[isleID].m_Fields[shopPosIndex];
            }
            else
            {
                shopPos = pos;
            }
            shopVec = new Vector3(shopPos.x * m_MapScaleX, 5, shopPos.y * m_MapScaleZ);

            switch (isles[isleID].Theme)
            {
                case BuildingRegistry.EIsleThemes.Wild:
                    {
                        newIsle = Instantiate(m_WildThemedBuildings.MainBuilding, vec, default, null).GetComponent<Isle>();
                        newIsle.m_ShipRegistry = m_ShipRegistry;                       
                    }break;
                case BuildingRegistry.EIsleThemes.Neutral:
                    {
                        GameRegistry.m_IsleCount++;
                        newIsle = Instantiate(m_NeutralThemedBuildings.MainBuilding, vec, default, null).GetComponent<Isle>();
                        Instantiate(m_NeutralThemedBuildings.ShipShop, shopVec, default, null).GetComponent<Shop>().m_Isle = newIsle;
                    }
                    break;
                case BuildingRegistry.EIsleThemes.British:
                    {
                        GameRegistry.m_IsleCount++;
                        newIsle = Instantiate(m_BritishThemedBuildings.MainBuilding, vec, default, null).GetComponent<Isle>();
                        Instantiate(m_BritishThemedBuildings.ShipShop, shopVec, default, null).GetComponent<Shop>().m_Isle = newIsle;
                    }
                    break;
            }
            newIsle.m_IsleCenter = new Vector2(isles[isleID].IsleCenter.x * m_MapScaleX, isles[isleID].IsleCenter.y * m_MapScaleZ);
            newIsle.SqrIsleRadius = (new Vector2(isles[isleID].IsleUpperLeftCorner.x * m_MapScaleX, isles[isleID].IsleUpperLeftCorner.y * m_MapScaleZ) - newIsle.m_IsleCenter).sqrMagnitude + 1800;
            newIsle.m_ShipRegistry = m_ShipRegistry;
            newIsle.m_GarnisionStrength = isles[isleID].m_Fields.Count / 2;
            newIsle.m_LootRising = isles[isleID].m_Fields.Count / 30;
            newIsle.m_PatrolStrength = newIsle.m_GarnisionStrength / 2;
            switch (isles[isleID].Theme)
            {
                case BuildingRegistry.EIsleThemes.Wild:
                    {
                        if (!playerShipSpawned)
                        {
                            Vector3 camPos = newIsle.Spawn(ShipBehaviour.EShiptype.Flagship, GameRegistry.EFractions.Pirates).transform.position;
                            playerShipSpawned = true;
                            CameraController.SetPosition = new Vector2(camPos.x, camPos.z);
                        }
                    }
                    break;
                case BuildingRegistry.EIsleThemes.Neutral:
                    {
                        newIsle.Fraction = GameRegistry.EFractions.Neutral;
                    }break;
                case BuildingRegistry.EIsleThemes.British:
                    {
                        newIsle.Fraction = GameRegistry.EFractions.British;
                        newIsle.m_NavMeshObstacle.center *= 13.3f; //to fix scaling issues for obstacle
                        newIsle.m_NavMeshObstacle.radius *= 13.3f; //to fix scaling issues for obstacle
                    }
                    break;
            }
        }

        //GameObject chunk;
        //Mesh mesh = MeshGenerator.GenerateMesh(2, 2, new float[CHUNCK_EDGE_SIZE, CHUNCK_EDGE_SIZE]);
        //for (int x = 0; x < m_MapSizeX * m_MapScaleX;  x+= (CHUNCK_EDGE_SIZE - 1) * 2 )
        //{
        //    for (int z = 0; z < m_MapSizeZ * m_MapScaleX; z+= (CHUNCK_EDGE_SIZE - 1) * 2)
        //    {
        //        chunk = Instantiate(m_ChunkPrefab, new Vector3(x, transform.position.y, z), Quaternion.identity, transform);
        //        chunk.GetComponent<MeshFilter>().mesh = mesh;
        //        chunk.GetComponent<MeshRenderer>().material = m_WaterMaterial;
        //    }
        //}

        GameObject chunk;
        Mesh mesh = MeshGenerator.GenerateMesh(2, 2, new float[CHUNCK_EDGE_SIZE, CHUNCK_EDGE_SIZE]);
        for (int x = (-((CHUNCK_EDGE_SIZE - 1) * 2) * 4); x < m_MapSizeX * m_MapScaleX + (((CHUNCK_EDGE_SIZE - 1) * 2) * 4); x += (CHUNCK_EDGE_SIZE - 1) * 2)
        {
            for (int z = (-((CHUNCK_EDGE_SIZE - 1) * 2) * 4); z < m_MapSizeZ * m_MapScaleZ + (((CHUNCK_EDGE_SIZE - 1) * 2) * 4); z += (CHUNCK_EDGE_SIZE - 1) * 2)
            {
                chunk = Instantiate(m_ChunkPrefab, new Vector3(x, transform.position.y, z), Quaternion.identity, transform);
                chunk.GetComponent<MeshFilter>().mesh = mesh;
                chunk.GetComponent<MeshRenderer>().material = m_WaterMaterial;
            }
        }

        #endregion Buildings
    }

    #region Mesh generation

    private void GenerateMap(float[,] _map)
    {
        MeshFilter mesh;

        float[,][,] chunks = GenerateChunks(_map);

        for (int x = 0; x < chunks.GetLength(0); x++)
        {
            for (int z = 0; z < chunks.GetLength(1); z++)
            {
                mesh = Instantiate(m_ChunkPrefab, new Vector3((x * CHUNCK_EDGE_SIZE) * m_MapScaleX - x *m_MapScaleX, transform.position.y - m_MapHeight * 0.75f, ((z * CHUNCK_EDGE_SIZE)) * m_MapScaleZ - z * m_MapScaleZ), Quaternion.identity, transform).GetComponent<MeshFilter>();
                mesh.mesh = MeshGenerator.GenerateMesh(m_MapScaleX, m_MapScaleZ, chunks[x, z]);
            }
        }
    }

    private float[,][,] GenerateChunks(float[,] _map)
    {
        float[,][,] chunks = new float[(_map.GetLength(0) / (CHUNCK_EDGE_SIZE - 1)) + 1, (_map.GetLength(1) / (CHUNCK_EDGE_SIZE - 1)) + 1][,];

        for (int x = 0; x < chunks.GetLength(0); x++)
        {
            for (int z = 0; z < chunks.GetLength(1); z++)
            {
                chunks[x, z] = GetSubmap(x * CHUNCK_EDGE_SIZE - x, z * CHUNCK_EDGE_SIZE - z, _map);
            }
        }

        return chunks;
    }

    private float[,] GetSubmap(int _startX, int _startZ, float[,] _map)
    {
        int sizeX = CHUNCK_EDGE_SIZE;
        int sizeZ = CHUNCK_EDGE_SIZE;

        if(_map.GetLength(0) - _startX < CHUNCK_EDGE_SIZE)
        {
            sizeX = _map.GetLength(0) - _startX;
        }

        if (_map.GetLength(1) - _startZ < CHUNCK_EDGE_SIZE)
        {
            sizeZ = _map.GetLength(1) - _startZ;
        }

        float[,] result = new float[sizeX, sizeZ];

        for (int x = 0; x < sizeX; x++)
        {
            for (int z = 0; z < sizeZ; z++)
            {
                result[x, z] = _map[_startX + x, _startZ + z];
            }
        }

        return result;
    }

    #endregion Mesh generation

    private bool CARuleset(ref bool[,] _gen, int _x, int _y)
    {
        if(_x <= 1 || _x >= _gen.GetLength(0) - 2 || _y <= 1 || _y >= _gen.GetLength(1) - 2)
        {
            return false;
        }

        int trueCounter = 0;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if((x != 0 || y != 0) && _gen[_x + x, _y + y])
                {
                    trueCounter++;   
                }
            }
        }
        if(trueCounter >= m_CaRequierdNeighbours)
        {
            return true;
        }
        return false;
    }

    private float[,] GenerateBaseMap()
    {
        m_CAMap = CellularAutomata.GenerateAutomata((int)m_MapSizeX, (int)m_MapSizeZ, m_CaTrueStart, m_CaInitStartGen, CARuleset, m_MapSeed);

        float[,] result = new float[(int)m_MapSizeX, (int)m_MapSizeZ];

        int trueCounter = 0;
        for (int x = 0; x < m_MapSizeX; x++)
        {
            for (int z = 0; z < m_MapSizeZ; z++)
            {
                if (m_CAMap[x, z])
                {
                    result[x, z] = 1;
                }
                else
                {
                    for (int localx = -2; localx < 2; localx++)
                    {
                        for (int localz = -2; localz < 2; localz++)
                        {
                            if (x + localx > 0 && x + localx < m_MapSizeX && z + localz > 0 && z + localz < m_MapSizeZ)
                            {
                                if (m_CAMap[x + localx, z + localz])
                                {
                                    trueCounter++;
                                }
                            }
                        }
                    }

                    result[x, z] = trueCounter / 25f;


                    trueCounter = 0;
                }
            }
        }

        return result;
    }

    private void OnDrawGizmos()
    {
        //if(m_CAMap == null)
        //{
        //    return;
        //}

        //for (int x = 0; x < m_CAMap.GetLength(0); x++)
        //{
        //    for (int y = 0; y < m_CAMap.GetLength(1); y++)
        //    {
        //        if(m_CAMap[x, y])
        //        {
        //            Gizmos.color = Color.black;
        //        }
        //        else 
        //        {
        //            Gizmos.color = Color.white;
        //        }
        //        Gizmos.DrawCube(new Vector3(x, 0, y), Vector3.one);
        //    }
        //}
    }

    private class MapIsle
    {
        int m_Minx = -1, m_MaxX = -1, m_MinZ = -1, m_MaxZ = -1;
        public List<Vector2Int> m_Fields = new List<Vector2Int>();

        public Vector2 IsleCenter
        {
            get { return new Vector2((m_Minx + m_MaxX) / 2f, (m_MinZ + m_MaxZ) / 2f); }
        }

        public Vector2 IsleUpperLeftCorner
        {
            get { return new Vector2(m_Minx, m_MinZ); }
        }

        public BuildingRegistry.EIsleThemes Theme
        {
            get 
            {
                if(m_Fields.Count > 10)
                {
                    return BuildingRegistry.EIsleThemes.British;
                }
                if(m_Fields.Count > 4)
                {
                    return BuildingRegistry.EIsleThemes.Neutral;
                }
                return BuildingRegistry.EIsleThemes.Wild;
            }
        }

        public bool Contains(Vector2Int _field)
        {
            return m_Fields.Contains(_field);
        }

        public void Add(Vector2Int _field)
        {
            if(m_Minx == -1)
            {
                m_Minx = m_MaxX = _field.x;
                m_MinZ = m_MaxZ = _field.y;
            }
            else
            {
                if(m_Minx > _field.x)
                {
                    m_Minx = _field.x;
                }
                else if(m_MaxX < _field.x)
                {
                    m_MaxX = _field.x;
                }

                if(m_MinZ > _field.y)
                {
                    m_MinZ = _field.y;
                }
                else if(m_MaxZ < _field.y)
                {
                    m_MaxZ = _field.y;
                }
            }
            
            m_Fields.Add(_field);
        }
    }
}