﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGenerater //Done by: Pascal
{
    Vector2[,] m_Vectors;

    public NoiseGenerater(int _x = 2, int _y = 2)
    {
        Resize(_x, _y);
        RandomizeVectores();       
    }

    public NoiseGenerater(int _seed, int _x = 2, int _y = 2)
    {
        Resize(_x, _y);
        RandomizeVectores(_seed);
    }

    public float this[float _x, float _y]
    {
        get
        {
            return GetValue(_x, _y);
        }
    }

    public float GetValue(float _x, float _y)
    {
        _x %= 1;
        _y %= 1;
        _x *= m_Vectors.GetLength(0) - 1;
        _y *= m_Vectors.GetLength(1) - 1;
        int UpperLeftX = (int)_x;
        int UpperLeftY = (int)_y;
        _x %= 1;
        _y %= 1;
        return (DotProduct(m_Vectors[UpperLeftX, UpperLeftY].x, m_Vectors[UpperLeftX, UpperLeftY].y, 1 - _x, 1 - _y) +
                DotProduct(m_Vectors[UpperLeftX + 1, UpperLeftY].x, m_Vectors[UpperLeftX + 1, UpperLeftY].y, _x, 1 - _y) +
                DotProduct(m_Vectors[UpperLeftX, UpperLeftY + 1].x, m_Vectors[UpperLeftX, UpperLeftY + 1].y, 1 - _x, _y) +
                DotProduct(m_Vectors[UpperLeftX + 1, UpperLeftY + 1].x, m_Vectors[UpperLeftX + 1, UpperLeftY + 1].y, _x, _y)) / 4;
    }

    private float DotProduct(float _x1, float _y1, float _x2, float _y2)
    {
        return _x1 * _x2 + _y1 * _y2;
    }

    public void Resize(int _x, int _y)
    {
        if (_x < 2)
        {
            _x = 2;
        }
        if (_y < 2)
        {
            _y = 2;
        }
        if (m_Vectors == null)
        {
            m_Vectors = new Vector2[_x, _y];
            return;
        }
        Vector2[,] newArray = new Vector2[_x, _y];
        for (int i = 0; i < newArray.GetLength(0) && i < m_Vectors.GetLength(0); i++)
        {
            for (int j = 0; j < newArray.GetLength(1) && j < m_Vectors.GetLength(1); j++)
            {
                newArray[i, j] = m_Vectors[i, j];
            }
        }
    }

    public void RandomizeVectores()
    {
        float x = 0, y = 0;
        for (int i = 0; i < m_Vectors.GetLength(0); i++)
        {
            for (int j = 0; j < m_Vectors.GetLength(1); j++)
            {
                do
                {
                    x = Random.Range(-1f, 1f);
                } while (x == 0);

                do
                {
                    y = Random.Range(-1f, 1f);
                } while (y == 0);

                m_Vectors[i, j].x = x;
                m_Vectors[i, j].y = y;
                m_Vectors[i, j].Normalize();
            }
        }
    }
    public void RandomizeVectores(int _seed)
    {
        Random.InitState(_seed);
        RandomizeVectores();
    }
}
