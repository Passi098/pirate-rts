﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HeightMap : NoiseMap //Done by: Pascal
{
    public uint m_Width = 100;

    public uint m_Depth = 100;

    public float m_MinHeight = 0;

    public float m_MaxHeight = 20;

    public float[] m_Smoothness = { 3, 3, 3, 3, 3 };

    public int m_Seed = 12345;

    public HeightMap()
    {

    }

    public HeightMap(uint _width, uint _depth, float _minHeight, float _maxHeight, float[] _smoothness, int _seed, bool _heightUp = true ,bool _init = true)
    {
        m_Width = _width;
        m_Depth = _depth;
        m_MinHeight = _minHeight;
        m_MaxHeight = _maxHeight;
        m_Smoothness = _smoothness;
        m_Seed = _seed;
        if (_init)
        {
            InitMap();
        }
        if(_heightUp)
        {
            BringHeight();
        }
    }

    public float HeightDifference
    {
        get
        {
            return m_MaxHeight - m_MinHeight;
        }
    }

    public void InitMap()
    {
        GenerateMultipleNoisemap(m_Width, m_Depth, m_Smoothness, m_Seed);
    }

    public void BringHeight()
    {
        for (int i = 0; i < m_Width; i++)
        {
            for (int j = 0; j < m_Depth; j++)
            {
                this[i, j] = this[i, j] * HeightDifference + m_MinHeight;
            }
        }
    }

    public void DrawHeightmap()
    {
        Vector3 a = Vector3.zero;
        Vector3 b = Vector3.zero;

        for (int i = 0; i < Width - 1; i++)
        {
            for (int j = 0; j < Depth - 1; j++)
            {
                a.x = i;
                a.z = j;
                a.y = this[i, j];

                b = a;
                b.x ++;
                b.y = this[i + 1, j];
                Gizmos.color = Color.Lerp(Color.blue, Color.cyan, b.y);
                Gizmos.DrawLine(a, b);

                b.z ++;
                b.y = this[i + 1, j + 1];
                Gizmos.color = Color.Lerp(Color.blue, Color.cyan, b.y);
                Gizmos.DrawLine(a, b);

                b.x = a.x;
                b.y = this[i, j + 1];
                Gizmos.color = Color.Lerp(Color.blue, Color.cyan, b.y);
                Gizmos.DrawLine(a, b);
            }
        }
    }
}
