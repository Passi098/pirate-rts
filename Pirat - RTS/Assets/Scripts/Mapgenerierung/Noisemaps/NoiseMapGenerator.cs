﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NoiseMapGenerator //Done by: Pascal
{
    public static float[,] GenerateNoisemap(uint _width, uint _depth, float _smoothness, int Seed)
    {
        int rndState = Random.Range(int.MinValue, int.MaxValue);
        Random.InitState(Seed);
        float[,] result = GenerateNoisemap(_width, _depth, _smoothness);
        Random.InitState(rndState);
        return result;
    }

    public static float[,] GenerateNoisemap(uint _width, uint _depth, float _smoothness)
    {
        if (_smoothness == 0)
        {
            return null;
        }
        int offsetX = Random.Range(0, (int)_width);
        int offsetY = Random.Range(0, (int)_depth);
        float[,]result = new float[_width, _depth];
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _depth; y++)
            {
                result[x, y] = Mathf.PerlinNoise((float)(x + offsetX) / (_width * 2) * _smoothness, (float)(y + offsetY) / (_depth * 2) * _smoothness);
            }
        }

        return result;
    }
}
