﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMap //Done by: Pascal
{
    public int Width
    {
        get 
        { 
            if (m_Map != null)
            {
                return m_Map.Length;
            }
            return 0;
        }
    }
    public int Depth
    {
        get 
        {
            if (Width > 0)
            {
                if (m_Map[0] != null)
                {
                    return m_Map[0].Length; 
                }
            }
            return 0;
        }
    }

    public int Count
    {
        get { return Width * Depth; }
    }

    private float[][] m_Map;

    public delegate float Modifierer(float _x, float _y, float _value);

    public NoiseMap()
    {

    }

    public NoiseMap(uint _width, uint _depth)
    {
        m_Map = new float[_width][];
        for (int i = 0; i < m_Map.Length; i++)
        {
            m_Map[i] = new float[_depth];
        }
    }

    public NoiseMap(uint _width, uint _depth, float _baseValue)
    {
        m_Map = new float[_width][];
        for (int i = 0; i < Width; i++)
        {
            m_Map[i] = new float[_depth];
            for (int j = 0; j < Depth; j++)
            {
                m_Map[i][j] = _baseValue;
            }
        }
    }

    public NoiseMap(NoiseMap _noiseMap)
    {
        m_Map = _noiseMap.Map;
    }

    /// <summary>
    /// multiplies 2 noisemaps with same demensions
    /// </summary>
    /// <param name="_a"></param>
    /// <param name="_b"></param>
    /// <returns></returns>
    public static NoiseMap operator * (NoiseMap _a, NoiseMap _b)
    {
        if(_a.Width != _b.Width || _a.Depth != _b.Depth)
        {
            return new NoiseMap();
        }
        NoiseMap resultMap = new NoiseMap((uint)_a.Width, (uint)_a.Depth);
        for (int i = 0; i < resultMap.Width; i++)
        {
            for (int j = 0; j < resultMap.Depth; j++)
            {
                resultMap[i, j] = _a[i, j] * _b[i, j];
            }
        }
        return resultMap;
    }

    public static NoiseMap operator * (NoiseMap _a, float _b)
    {
        NoiseMap resultMap = new NoiseMap((uint)_a.Width, (uint)_a.Depth);
        for (int i = 0; i < resultMap.Width; i++)
        {
            for (int j = 0; j < resultMap.Depth; j++)
            {
                resultMap[i, j] = _a[i, j] * _b;
            }
        }
        return resultMap;
    }

    public float this[int _x, int _y]
    {
        get 
        {
                return m_Map[_x][_y];
        }
        set 
        {
                m_Map[_x][_y] = value;
        }
    }

    public float[][] Map
    {
        get { return m_Map; }
    }

    public void Modify(Modifierer _mod)
    {
        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Depth; j++)
            {
                m_Map[i][j] = _mod(i / (float)Width, j / (float)Depth, m_Map[i][j]);
            }
        }
    }

    public void Resize(uint _sizeX, uint _sizeY, uint _startX = 0, uint _startY = 0)
    {
        float[][] newMap = new float[_sizeX][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[_sizeY];
        }

        for (uint i = 0; i < _sizeX && i + _startX < m_Map.Length; i++)
        {
            for (uint j = 0; j < _sizeY && j + _startY < m_Map[i].Length; j++)
            {
                newMap[i][j] = m_Map[i + _startX][j + _startY];
            }
        }
        m_Map = newMap;
    }

    public void MoveMap(int _x, int _y)
    {
        float[][] newMap = new float[m_Map.Length][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[m_Map[i].Length];
        }
        for (int i = 0; i < m_Map.Length; i++)
        {
            for (int j = 0; j < m_Map[i].Length; j++)
            {
                if(i + _x < 0 || i + _x > m_Map.Length ||j + _y < 0 || j + _y > m_Map[i].Length)
                {
                    newMap[i][j] = 0;
                }
                else
                {
                    newMap[i][j] = m_Map[i + _x][j + _y];
                }
            }
        }
        m_Map = newMap;
    }

    public void ScaleMap(int _x, int _y)
    {
        if(_x == 0 || _y == 0)
        {
            m_Map = new float[0][];
            return;
        }
        int base_x = _x;
        int base_y = _y;
        _x = Mathf.Abs(_x);
        _y = Mathf.Abs(_y);
        float[][] newMap = new float[m_Map.Length * _x][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[m_Map[i / _x].Length * _y];
        }

        for (int i = 0; i < newMap.Length; i++)
        {
            for (int j = 0; j < newMap[i].Length; j++)
            {
                newMap[i][j] = m_Map[i / _x][j / _y];
            }
        }
        m_Map = newMap;

        if(base_x < 0)
        {
            for (int i = 0; i < m_Map.Length; i++)
            {
                for (int j = 0; j < m_Map.Length; j++)
                {
                    m_Map[i][j] = m_Map[m_Map.Length - 1 - i][j];
                }
            }
        }
        if (base_y < 0)
        {
            for (int i = 0; i < m_Map.Length; i++)
            {
                for (int j = 0; j < m_Map.Length; j++)
                {
                    m_Map[i][j] = m_Map[i][m_Map[i].Length - 1 - j];
                }
            }
        }
    }

    public void ScaleHeight(float _scale)
    {
        for (int i = 0; i < m_Map.Length; i++)
        {
            for (int j = 0; j < m_Map[i].Length; j++)
            {
                m_Map[i][j] *= _scale;
            }
        }
    }

    public  void ScaleHeight(NoiseMap _scaleMap)
    {
        if (Width != _scaleMap.Width || Depth != _scaleMap.Depth)
        {
            return;
        }
        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Depth; j++)
            {
                this[i, j] = this[i, j] * _scaleMap[i, j];
            }
        }
    }

    public void TurnLeft()
    {
        float[][] newMap = new float[m_Map[0].Length][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[m_Map.Length];
        }

        for (int i = 0; i < m_Map.Length; i++)
        {
            for (int j = 0; j < m_Map[i].Length; j++)
            {
                newMap[j][m_Map.Length - 1 - i] = m_Map[i][j];
            }
        }
    }

    public void TurnRight()
    {
        float[][] newMap = new float[m_Map[0].Length][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[m_Map.Length];
        }

        for (int i = 0; i < m_Map.Length; i++)
        {
            for (int j = 0; j < m_Map[i].Length; j++)
            {
                newMap[m_Map[i].Length - 1 -j][i] = m_Map[i][j];
            }
        }
    }

    public void TurnUpsideDown()
    {
        float[][] newMap = new float[m_Map[0].Length][];
        for (int i = 0; i < newMap.Length; i++)
        {
            newMap[i] = new float[m_Map.Length];
        }

        for (int i = 0; i < m_Map.Length; i++)
        {
            for (int j = 0; j < m_Map[i].Length; j++)
            {
                newMap[m_Map[i].Length - 1 - j][m_Map.Length - 1 - i] = m_Map[i][j];
            }
        }
    }

    public void TurnFrontBack()
    {
        ScaleHeight(-1);
    }

    public void Invert()
    {
        for (int i = 0; i < Width; i++)
        {
            for (int j = 0; j < Depth; j++)
            {
                this[i, j] = 1 / this[i, j];
            }
        }
    }

    public NoiseMap Inverse
    {
        get 
        {
            NoiseMap resultMap = new NoiseMap((uint)Width, (uint)Depth);
            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Depth; j++)
                {
                    resultMap[i, j] = 1 / this[i, j];
                }
            }
            return resultMap;
        }
    }

    public void GenerateNoisemap(uint _width, uint _depth, float _smoothness, int Seed)
    {
        Random.InitState(Seed);
        GenerateNoisemap(_width, _depth, _smoothness);
    }
    public void GenerateNoisemap(uint _width, uint _depth, float _smoothness)
    {
        if (_smoothness == 0)
        {
            _smoothness = 1;
        }
        m_Map = new float[_width * 2][];
        for (int i = 0; i < Width; i++)
        {
            m_Map[i] = new float[_depth * 2];
            for (int j = 0; j < Depth; j++)
            {
                m_Map[i][j] = Mathf.PerlinNoise((float)i / Width * _smoothness, (float)j / Depth * _smoothness);
            }
        }

        Resize(_width, _depth, (uint)Random.Range(0, (int)_width + 1), (uint)Random.Range(0, (int)_depth + 1));
    }

    public void GenerateNoisemap( uint _width, uint _depth, float _smoothness, NoiseGenerater _generater)
    {
        if (_smoothness == 0)
        {
            _smoothness = 1;
        }
        m_Map = new float[_width * 2][];
        for (int i = 0; i < Width; i++)
        {
            m_Map[i] = new float[_depth * 2];
            for (int j = 0; j < Depth; j++)
            {
                m_Map[i][j] = _generater[(float)i / Width * _smoothness, (float)j / Depth * _smoothness];
            }
        }

        Resize(_width, _depth, (uint)Random.Range(0, (int)_width + 1), (uint)Random.Range(0, (int)_depth + 1));
    }

    public void GenerateMultipleNoisemap(uint _width, uint _depth, float[] _smoothness, int _seed)
    {
        Random.InitState(_seed);
        GenerateMultipleNoisemap(_width, _depth, _smoothness);
    }
    public void GenerateMultipleNoisemap(uint _width, uint _depth, float[] _smoothness)
    {
        GenerateNoisemap(_width, _depth, _smoothness[0]);
        NoiseMap noisemap = new NoiseMap();

        for (int i = 1; i < _smoothness.Length; i++)
        {
            noisemap.GenerateNoisemap(_width, _depth, _smoothness[i]);

            switch (Random.Range(1, 3))
            {
                case 2:
                    {
                        noisemap.TurnLeft();
                        break;
                    }

                case 3:
                    {
                        noisemap.TurnRight();
                        break;
                    }
            }

            if(Random.Range(1, 3) > 1)
            {
                noisemap.TurnUpsideDown();
            }
            ScaleHeight(noisemap);
        

            float placeHolder = 0;
            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    if (m_Map[j][k] > placeHolder)
                    {
                        placeHolder = m_Map[j][k];
                    }
                }
            }
            if( placeHolder == 0)
            {
                return;
            }
            placeHolder = 1 / placeHolder;

            ScaleHeight(placeHolder);

            placeHolder = 1;

            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    if (m_Map[j][k] < placeHolder)
                    {
                        placeHolder = m_Map[j][k];
                    }
                }
            }

            if(placeHolder == 0)
            {
                return;
            }

            float minHeight = placeHolder;
            placeHolder = placeHolder / 1;

            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    m_Map[j][k] *= (1 - ((m_Map[j][k] - minHeight) / (1 - minHeight))) * placeHolder;
                }
            }

        }
    }

    public void GenerateMultipleNoisemap(uint _width, uint _depth, float[] _smoothness, int _seed, NoiseGenerater _generater)
    {
        Random.InitState(_seed);
        GenerateMultipleNoisemap(_width, _depth, _smoothness, _generater);
    }
    public void GenerateMultipleNoisemap(uint _width, uint _depth, float[] _smoothness, NoiseGenerater _generater)
    {
        GenerateNoisemap(_width, _depth, _smoothness[0]);
        NoiseMap noisemap = new NoiseMap();

        for (int i = 1; i < _smoothness.Length; i++)
        {
            noisemap.GenerateNoisemap(_width, _depth, _smoothness[i], _generater);

            switch (Random.Range(1, 3))
            {
                case 2:
                    {
                        noisemap.TurnLeft();
                        break;
                    }

                case 3:
                    {
                        noisemap.TurnRight();
                        break;
                    }
            }

            if(Random.Range(1, 3) > 1)
            {
                noisemap.TurnUpsideDown();
            }
            ScaleHeight(noisemap);
        

            float placeHolder = 0;
            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    if (m_Map[j][k] > placeHolder)
                    {
                        placeHolder = m_Map[j][k];
                    }
                }
            }
            if( placeHolder == 0)
            {
                return;
            }
            placeHolder = 1 / placeHolder;

            ScaleHeight(placeHolder);

            placeHolder = 1;

            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    if (m_Map[j][k] < placeHolder)
                    {
                        placeHolder = m_Map[j][k];
                    }
                }
            }

            if(placeHolder == 0)
            {
                return;
            }

            float minHeight = placeHolder;
            placeHolder = placeHolder / 1;

            for (int j = 0; j < _width; j++)
            {
                for (int k = 0; k < _depth; k++)
                {
                    m_Map[j][k] *= (1 - ((m_Map[j][k] - minHeight) / (1 - minHeight))) * placeHolder;
                }
            }

        }
    }
}
