﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CellularAutomata //Done by: Pascal
{
    public delegate bool Ruleset(ref bool[,] _gen, int _xPos, int _yPos);

    public static bool[,] GenerateAutomata(int _sizeX, int _sizeY, int _startTruePercent, int _generations, Ruleset _rules, int Seed)
    {
        int rndState = Random.seed;
        Random.InitState(Seed);

        bool[,] result = GenerateAutomata(_sizeX, _sizeY, _startTruePercent, _generations, _rules);

        Random.InitState(rndState);
        return result;
    }

    public static bool[,] GenerateAutomata(int _sizeX, int _sizeY, int _startTruePercent, int _generations, Ruleset _rules)
    {
        bool[,] result = new bool[_sizeX, _sizeY];

        for (int x = 0; x < _sizeX; x++)
        {
            for (int y = 0; y < _sizeY; y++)
            {
                if (Random.Range(0, 100) < _startTruePercent)
                {
                    result[x, y] = true;
                }
                else
                {
                    result[x, y] = false;
                }
            }
        }

        return NextGen(result, _rules, _generations);
    }

    public static bool[,] NextGen(bool[,] _currGen, Ruleset _rules, int _generations = 1)
    {
        bool[,] nextGen = new bool[_currGen.GetLength(0), _currGen.GetLength(1)];

        for (int gen = 0; gen < _generations; gen++)
        {
            for (int x = 0; x < _currGen.GetLength(0); x++)
            {
                for (int y = 0; y < _currGen.GetLength(0); y++)
                {
                    nextGen[x, y] = _rules(ref _currGen, x, y);
                }
            }
            _currGen = nextGen;
        }

        return _currGen;
    }
}
