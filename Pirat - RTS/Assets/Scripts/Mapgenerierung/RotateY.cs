﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateY : MonoBehaviour //Done by: Pascal
{
    [SerializeField]
    private float m_Speed = 10;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, m_Speed * Time.deltaTime, 0);
    }
}
