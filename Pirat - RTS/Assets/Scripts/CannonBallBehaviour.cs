﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallBehaviour : MonoBehaviour //Lukas
{
    private const float FORWARD_SPEED = 20f;
    private const float HEIGHT_SCALE = 3.5f;
    private const float DESPAWN_DELAY = 5f;

    [SerializeField]
    AnimationCurve m_ShootingCurve;

    /// <summary>
    /// displays the cannon movement in a customized curve
    /// </summary>
    /// <param name="_firePoint">starting point of the shot</param>
    /// <returns></returns>
    public IEnumerator CannonBallMovement(Transform _firePoint)
    {
        float timer = DESPAWN_DELAY;
        do
        {
            timer -= Time.deltaTime; // since it's looping frame for frame, it just substracts Time.deltaTime

            transform.position += _firePoint.transform.forward * FORWARD_SPEED * Time.deltaTime;
            transform.position += _firePoint.transform.up * HEIGHT_SCALE * (m_ShootingCurve.Evaluate(timer / DESPAWN_DELAY)) * Time.deltaTime;

            yield return null;
        } while (timer > 0);//loops till the cannon shall vanish

        Destroy(gameObject);
    }
}
