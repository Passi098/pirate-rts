﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class GameRegistry //Done by: Pascal & Lukas
{
    public enum EFractions { Pirates, Neutral, British }
    public enum ESelectableType { Ship, Shop }

    public static int m_MapSeed = 18;

    public static int m_IsleCount;

    public static float m_PlayerGold = 1000;

    public static List<ShipBehaviour> m_PlayerShips = new List<ShipBehaviour>();

    public static List<Isle> m_PlayersIsles = new List<Isle>();

    public static void RemoveShipToPlayerShips(ShipBehaviour _ship)
    {
        m_PlayerShips.Remove(_ship);
        GameLost();
    }

    public static void AddShipsToPlayerShips(ShipBehaviour _ship)
    {
        m_PlayerShips.Add(_ship);
    }

    public static void RemoveIsleFromPlayerIsles(Isle _isle)
    {
        m_PlayersIsles.Remove(_isle);
        GameLost();
    }

    public static void AddIsleToPlayerIsles(Isle _isle)
    {
        m_PlayersIsles.Add(_isle);
        GameWon();
    }

    private static void GameWon()
    {
        if(m_IsleCount == 0)
        {
            return;
        }
        if (m_PlayersIsles.Count >= m_IsleCount)
        {
            SceneManager.LoadScene(2); //Load win scene
        }
    }

    private static void GameLost()
    {
        if (m_PlayerShips.Count <= 0 && m_PlayersIsles.Count <= 0)
        {
            SceneManager.LoadScene(3); //Load lose scene
        }
    }
}
