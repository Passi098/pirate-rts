﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class KeyBindings //Done by: Pascal & Lukas
{
    public static KeyCode m_SelecterSelect = KeyCode.Mouse0;
    public static KeyCode m_SelecterSetDestination = KeyCode.Mouse1;
    public static KeyCode m_SelecterMultiSelection = KeyCode.LeftControl;

    public static KeyCode m_CamSpeedUp = KeyCode.LeftShift;
    public static KeyCode m_CamForward = KeyCode.W;
    public static KeyCode m_CamBackward = KeyCode.S;
    public static KeyCode m_CamLeft = KeyCode.A;
    public static KeyCode m_CamRight = KeyCode.D;
    public static KeyCode m_CamRotate = KeyCode.Mouse2;
}
