﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidShark : FishBoid //Done by: Pascal
{
    protected override float Agility
    {
        get { return 0.5f; }
    }

    protected override Vector3 Velocity
    {
        get { return transform.forward * 1.5f; }
    }

    protected override float AllignmentMultiplier
    {
        get { return -2f * m_Boids.Count; }
    }
    protected override float CohesionMultiplier
    {
        get { return 0f; }
    }
    protected override float EffectivityRange
    {
        get { return 15f; }
    }
    protected override float SeperationMultiplier
    {
        get { return 1f * m_Boids.Count; }
    }

}
