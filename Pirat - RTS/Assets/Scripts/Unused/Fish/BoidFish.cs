﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidFish : FishBoid //Done by: Pascal
{
    protected override float Agility
    {
        get { return 0.1f; }
    }

    protected override float AllignmentMultiplier
    {
        get { return 1f; }
    }
    protected override float CohesionMultiplier
    {
        get { return 1f; }
    }
    protected override float EffectivityRange
    {
        get { return 30f; }
    }
    protected override float SeperationMultiplier
    {
        get { return 1f; }
    }

}
