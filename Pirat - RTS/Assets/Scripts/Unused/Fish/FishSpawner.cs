﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishSpawner : MonoBehaviour //Done by: Pascal
{
    [SerializeField]
    private float m_FishAmount = 10;

    [SerializeField]
    private float m_SpawnDistance = 20;

    [SerializeField]
    private GameObject m_FishPrefab;

    private void Awake()
    {
        for (int i = 0; i < m_FishAmount; i++)
        {
            Instantiate(m_FishPrefab, transform.position + Random.onUnitSphere * Random.Range(0, m_SpawnDistance), Random.rotation, transform);
        }
    }
}
