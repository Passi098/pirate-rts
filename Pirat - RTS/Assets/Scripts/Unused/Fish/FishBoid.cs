﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FishBoid : MonoBehaviour //Done by: Pascal
{
    protected static List<FishBoid> m_Boids = new List<FishBoid>();
    private static Vector3 m_SeperationStorage = Vector3.zero;
    private static Vector3 m_AllignmentStorage = Vector3.zero;
    private static Vector3 m_CohesionStorage = Vector3.zero;
    private static int m_Counter = 0;

    private static bool m_CoroutineIsRunning = false;
    private static int m_AgilityMultiplier = 1;


    private static IEnumerator FishBehaviour()
    {
        m_CoroutineIsRunning = true;
        int calculateToBoid = 0;
        while (m_Boids.Count > 0)
        {
            calculateToBoid += 10;
            for (int i = 0; i < m_Boids.Count && i < calculateToBoid; i++)
            {
                m_Boids[i].RacalcVelocity();
            }
            if(calculateToBoid >= m_Boids.Count)
            {
                calculateToBoid -= m_Boids.Count;
            }
            yield return new WaitForSeconds(0.1f);
        }
        m_CoroutineIsRunning = false;
    }

    protected virtual float Agility
    {
        get { return 1f; }
    }

    protected virtual Vector3 Velocity
    {
        get { return transform.forward; }
    }

    protected virtual float EffectivityRange
    {
        get { return 1; }
    }

    private float SqrEffectivityRange
    {
        get { return EffectivityRange * EffectivityRange; }
    }

    protected virtual float SeperationMultiplier
    {
        get { return 1; }
    }

    protected virtual float AllignmentMultiplier
    {
        get { return 1; }
    }

    protected virtual float CohesionMultiplier
    {
        get { return 1; }
    }

    private void Awake()
    {
        m_Boids.Add(this);
        m_AgilityMultiplier = 1 + (m_Boids.Count / 10);
        if(!m_CoroutineIsRunning)
        {
            StartCoroutine(FishBehaviour());
        }
    }

    private void RacalcVelocity()
    {
        for (int i = 0; i < m_Boids.Count; i++)
        {
            if (m_Boids[i] != this && (m_Boids[i].transform.position - transform.position).sqrMagnitude < m_Boids[i].SqrEffectivityRange)
            {
                m_Counter++;
                //m_CohesionStorage += (m_Boids[i].transform.position - transform.position) * ((m_Boids[i].transform.position - transform.position).sqrMagnitude / (m_Boids[i].SqrEffectivityRange)) * m_Boids[i].CohesionMultiplier;
                m_CohesionStorage += (m_Boids[i].transform.position - transform.position) * m_Boids[i].CohesionMultiplier;
                m_AllignmentStorage += m_Boids[i].Velocity * m_Boids[i].AllignmentMultiplier * (((m_Boids[i].transform.position - transform.position).sqrMagnitude * 1.5f) / (m_Boids[i].SqrEffectivityRange));
                m_SeperationStorage += (transform.position - m_Boids[i].transform.position) * ((-(m_Boids[i].transform.position - transform.position).sqrMagnitude / (m_Boids[i].SqrEffectivityRange * 0.5f)) + 1f) * m_Boids[i].SeperationMultiplier;
                //Nach - Von
            }
        }

        if (m_Counter > 0)
        {
            m_CohesionStorage /= m_Counter;
            //m_CohesionStorage = m_CohesionStorage - transform.position;
            m_AllignmentStorage /= m_Counter;
            m_SeperationStorage /= m_Counter;

            transform.rotation = Quaternion.LookRotation((((m_CohesionStorage + m_AllignmentStorage + m_SeperationStorage) / 3) * Agility * m_AgilityMultiplier * Time.fixedDeltaTime + Velocity) / 2);

            m_Counter = 0;
            m_CohesionStorage.x = m_CohesionStorage.y = m_CohesionStorage.z = 0;
            m_AllignmentStorage.x = m_AllignmentStorage.y = m_AllignmentStorage.z = 0;
            m_SeperationStorage.x = m_SeperationStorage.y = m_SeperationStorage.z = 0;
        }
    }

    private void FixedUpdate()
    {
        transform.position += Velocity * Time.fixedDeltaTime;
    }

    private void OnDestroy()
    {
        m_Boids.Remove(this);
    }
}
