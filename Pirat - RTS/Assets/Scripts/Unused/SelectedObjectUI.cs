﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedObjectUI : MonoBehaviour //Done by: Pascal
{
    private static SelectedObjectUI m_Instance;

    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(this);
            return;
        }
        m_Instance = this;
    }

    public static void Activate(Selectable[] _selectedObjects, int _lastSelected)
    {
        if(m_Instance == null)
        {
            return;
        }
        // TODO: UI 
    }
}
