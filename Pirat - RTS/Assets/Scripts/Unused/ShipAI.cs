﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CannonController))]
public class ShipAI : MonoBehaviour //Lukas
{
    [SerializeField]
    private GameObject Target;

    private CannonController m_Cannons;

    private float m_ToTargetAngle;
    private float m_FromTargetAngle;

    private void Awake()
    {
        m_Cannons = GetComponent<CannonController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Target == null)
        {
            return;
        }

        m_ToTargetAngle   = SignedAngle(transform.forward, Target.transform.position, Vector3.up);
        m_FromTargetAngle = SignedAngle(Target.transform.forward, transform.position, Vector3.up);
        //FireCannons();
    }

    //private void FireCannons()
    //{
    //    if(m_ToTargetAngle <= -85 && m_ToTargetAngle >= -95)
    //    {
    //        m_Cannons.FireLeftBrightSide();
    //    }
    //    else if (m_ToTargetAngle >= 85 && m_ToTargetAngle <= 95)
    //    {
    //        m_Cannons.FireRightBrightSide();
    //    }
    //    else if (m_ToTargetAngle >= -5 && m_ToTargetAngle <= 5)
    //    {
    //        m_Cannons.FireFrontCannons();
    //    }
    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="normal"></param>
    /// <returns>returns Angle from -180° left to 180° right</returns>
    public static float SignedAngle(Vector3 from, Vector3 to, Vector3 normal)
    {
        // angle in [0,180]
        return Vector3.Angle(from, to) * Mathf.Sign(Vector3.Dot(normal, Vector3.Cross(from, to)));
    }
}
