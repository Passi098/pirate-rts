﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SailController : MonoBehaviour //Lukas
{
    [SerializeField]
    public GameObject[] m_OpenSails = new GameObject[default];

    [SerializeField]
    public GameObject[] m_CloseSails = new GameObject[default];

    private bool m_SailsOpen = false;

    [SerializeField]
    private float m_Speed;

    //bool to control all sails
    private bool OpenSail
    {
        get
        {
            return m_SailsOpen;
        }
        set
        {
            if(value == m_SailsOpen)
            {
                return;
            }
            m_SailsOpen = value;
            for (int i = 0; i < m_OpenSails.Length; i++)
            {
                m_OpenSails[i].SetActive(m_SailsOpen); //open sails
            }
            for (int i = 0; i < m_CloseSails.Length; i++)
            {
                m_CloseSails[i].SetActive(!m_SailsOpen); //close sails
            } 
        }
    }
}
