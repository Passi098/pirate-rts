﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipGUI : MonoBehaviour //Done by: Pascal
{
    [SerializeField]
    private Canvas m_Canvas;

    [SerializeField]
    private Image m_HpBarFront;

    [SerializeField]
    private Image m_HpBarBack;

    [SerializeField]
    private Image m_AmmoBar;

    [SerializeField]
    private Image m_CrewBar;

    private Coroutine m_HpBackLoweringCoroutine;

    private const float BAR_LOWERING_SPEED = 0.2f;

    public bool Enabled
    {
        get { return m_Canvas.enabled; }
        set { m_Canvas.enabled = value; }
    }

    public float HP
    {
        set
        {
            value = Mathf.Clamp(value, 0, 1);
            if(value > m_HpBarBack.fillAmount)
            {
                m_HpBarBack.fillAmount = value;
            }
            m_HpBarFront.fillAmount = value;
            if (m_HpBackLoweringCoroutine != null)
            {
                StopCoroutine(m_HpBackLoweringCoroutine);
            }
            m_HpBackLoweringCoroutine = StartCoroutine(LowerHPBack());
        }
        get { return m_HpBarFront.fillAmount; }
    }

    private IEnumerator LowerHPBack()
    {
        while (m_HpBarFront.fillAmount < m_HpBarBack.fillAmount)
        {
            if(m_HpBarBack.fillAmount <= m_HpBarFront.fillAmount)
            {
                m_HpBarBack.fillAmount = m_HpBarFront.fillAmount;
                StopCoroutine(m_HpBackLoweringCoroutine);
            }
            m_HpBarBack.fillAmount = Mathf.Clamp(m_HpBarBack.fillAmount - (BAR_LOWERING_SPEED * Time.unscaledDeltaTime), m_HpBarFront.fillAmount, 1);   
            yield return null;
        }
    }

    public float Ammo
    {
        set
        {
            value = Mathf.Clamp(value, 0, 1);
            m_AmmoBar.fillAmount = value;
        }
        get { return m_AmmoBar.fillAmount; }
    }

    public float Crew
    {
        set
        {
            value = Mathf.Clamp(value, 0, 1);
            m_CrewBar.fillAmount = value;
        }
        get { return m_CrewBar.fillAmount; }
    }
}
