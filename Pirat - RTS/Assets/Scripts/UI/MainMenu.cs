﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour //Lukas
{
    [SerializeField]
    private GameObject m_Background;
    [SerializeField]
    private GameObject m_BackgroundCredits;
    [SerializeField]
    private GameObject m_BackgroundOptions;
    [SerializeField]
    private GameObject m_BackgroundSeedMenu;
    [SerializeField]
    private Slider m_Slider;
    [SerializeField]
    TMPro.TMP_Text m_SeedText;

    [SerializeField]
    private bool m_IsMainMenu;

    private void Update()
    {
        if (m_IsMainMenu)
        {
            m_SeedText.text = m_Slider.value.ToString();
        }
    }

    public void LoadScene(int _sceneIndex)
    {
        SceneManager.LoadScene(_sceneIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenCredits()
    {
        m_Background.SetActive(false);
        m_BackgroundCredits.SetActive(true);
    }

    public void CloseCredits()
    {
        m_Background.SetActive(true);
        m_BackgroundCredits.SetActive(false);
    }

    public void OpenOptions()
    {
        m_Background.SetActive(false);
        m_BackgroundOptions.SetActive(true);
    }

    public void CloseOptions()
    {
        m_Background.SetActive(true);
        m_BackgroundOptions.SetActive(false);

    }

    public void OpenSeedMenu()
    {
        m_Background.SetActive(false);
        m_BackgroundSeedMenu.SetActive(true);
    }

    public void CloseSeedMenu()
    {
        m_Background.SetActive(true);
        m_BackgroundSeedMenu.SetActive(false);
    }

    public void StartGame()
    {
        GameRegistry.m_MapSeed = (int)m_Slider.value;
        LoadScene(1);//load game loop scene
    }
}
