﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Minimap : MonoBehaviour //Done by: Pascal
{
    private static Minimap m_Instance;

    private Image m_Image;

    [SerializeField]
    private Image m_ButtonImage;

    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        m_Instance = this;
        m_Image = GetComponent<Image>();
        transform.parent.gameObject.SetActive(false);
    }

    public static Sprite SetImage
    {
        set 
        { 
            m_Instance.m_Image.sprite = value;
            m_Instance.m_ButtonImage.sprite = value;
        }
    }

    public void OpenMap()
    {
        transform.parent.gameObject.SetActive(!transform.parent.gameObject.activeSelf);
    }
}
