﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour //Lukas
{
    private bool m_IsPaused;
    [SerializeField]
    private GameObject m_PauseMenuUI;
    [SerializeField]
    private TMPro.TMP_Text m_GoldText;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (m_IsPaused == false)
            {
                Pause();
            }
            else
            {
                Resume();
            }
        }

        m_GoldText.text = ((int)GameRegistry.m_PlayerGold).ToString();
    }

    /// <summary>
    /// End PauseMenu
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1f;//Time set to normal
        m_IsPaused = false;
        m_PauseMenuUI.SetActive(false);//disable PauseMenu
    }

    /// <summary>
    /// Call Pause Menu
    /// </summary>
    public void Pause()
    {
        Time.timeScale = 0f;//Freeze Time
        m_IsPaused = true;
        m_PauseMenuUI.SetActive(true);//Set PauseMenu active
    }

    /// <summary>
    /// Exit Game
    /// </summary>
    public void ExitGame()
    {
        Application.Quit();
    }
}
