﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyMenu : MonoBehaviour //Lukas
{
    private static BuyMenu m_Instance;
    public List<Texture2D> m_ShipImageList = new List<Texture2D>();
    public List<string> m_NameList = new List<string>();
    public List<int> m_PrizeList = new List<int>();
    public List<GameObject> m_PrefabList = new List<GameObject>();

    [SerializeField]
    TMPro.TMP_Text m_ShipName;
    [SerializeField]
    TMPro.TMP_Text m_Prize;
    [SerializeField]
    RawImage m_DisplayedImage;
    private int m_ShipIndex = 0;

    public static Shop m_ActiveShop;


    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        m_Instance = this;
        gameObject.SetActive(false);
    }

    public static bool IsOpen
    {
        get { return m_Instance.gameObject.activeSelf; }
    }

    public static void Open()
    {
        m_Instance.gameObject.SetActive(true);
    }

    public static void Close()
    {
        m_Instance.gameObject.SetActive(false);
    }

    private void Update()
    {
        m_DisplayedImage.texture = m_ShipImageList[m_ShipIndex];
        m_ShipName.text = m_NameList[m_ShipIndex];
        m_Prize.text = "Prize: " + m_PrizeList[m_ShipIndex];
    }

    /// <summary>
    /// Swap to next ship and show its name, prize and picture
    /// </summary>
    public void ForwardClick()
    {
        if (m_ShipIndex < m_ShipImageList.Count - 1)
        {
            m_ShipIndex++;
        }
        else
        {
            m_ShipIndex = 0;
        }
    }

    /// <summary>
    /// Swap to last ship and show its name, prize and picture
    /// </summary>
    public void BackClick()
    {
        if (m_ShipIndex > 0)
        {
            m_ShipIndex--;
        }
        else
        {
            m_ShipIndex = m_ShipImageList.Count - 1;
        }
    }

    public void BuySelectedShip()
    {
        if(m_ActiveShop != null)
        {
            switch (m_ShipIndex)
            {
                case 0:
                    {
                        m_ActiveShop.Buy(ShipBehaviour.EShiptype.Shallop, m_PrizeList[m_ShipIndex]);
                    }break;

                case 1:
                    {
                        m_ActiveShop.Buy(ShipBehaviour.EShiptype.Cannonboat, m_PrizeList[m_ShipIndex]);
                    }break;

                case 2:
                    {
                        m_ActiveShop.Buy(ShipBehaviour.EShiptype.Galleon, m_PrizeList[m_ShipIndex]);
                    }break;
            }
            
        }
    }


}
