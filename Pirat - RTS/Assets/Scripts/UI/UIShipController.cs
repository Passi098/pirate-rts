﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShipController : MonoBehaviour //Lukas
{
    [SerializeField]
    float m_Speed;
    float m_ResetBorder = 175f;

    Vector3 m_StartPoint;

    private void Awake()
    {
        m_StartPoint = transform.position;
    }

    private void Update()
    {
        MoveShip();
    }

    /// <summary>
    /// Basic movement + reset to show ships in background
    /// </summary>
    private void MoveShip()
    {
        if (transform.position.x >= m_ResetBorder)
        {
            transform.position = m_StartPoint;
        }
        transform.position += transform.forward * m_Speed * Time.deltaTime;
    }
}
